// -*- mode: java -*- 
//
// file: cool-tree.m4
//
// This file defines the AST
//
//////////////////////////////////////////////////////////

import java.util.Enumeration;
import java.io.PrintStream;
import java.util.Vector;


/** Defines simple phylum Program */
abstract class Program extends TreeNode {
    protected Program(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);
    public abstract void semant();

}


/** Defines simple phylum Class_ */
abstract class Class_ extends TreeNode {
    protected Class_(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);
    public abstract void semant(SymbolTable st,ClassTable ct);
}


/** Defines list phylum Classes
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Classes extends ListNode {
    public final static Class elementClass = Class_.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Classes(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Classes" list */
    public Classes(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Class_" element to this list */
    public Classes appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Classes(lineNumber, copyElements());
    }
}


/** Defines simple phylum Feature */
abstract class Feature extends TreeNode {
    protected Feature(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);
    public abstract void semant(SymbolTable st,ClassTable ct);

}


/** Defines list phylum Features
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Features extends ListNode {
    public final static Class elementClass = Feature.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Features(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Features" list */
    public Features(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Feature" element to this list */
    public Features appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Features(lineNumber, copyElements());
    }
}


/** Defines simple phylum Formal */
abstract class Formal extends TreeNode {
    protected Formal(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);
    public abstract void semant(SymbolTable st,ClassTable ct);

}


/** Defines list phylum Formals
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Formals extends ListNode {
    public final static Class elementClass = Formal.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Formals(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Formals" list */
    public Formals(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Formal" element to this list */
    public Formals appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Formals(lineNumber, copyElements());
    }
}


/** Defines simple phylum Expression */
abstract class Expression extends TreeNode {
    protected Expression(int lineNumber) {
        super(lineNumber);
    }
    private AbstractSymbol type = null;                                 
    public AbstractSymbol get_type() { return type; }           
    public Expression set_type(AbstractSymbol s) { type = s; return this; } 
    public abstract void dump_with_types(PrintStream out, int n);
    public void dump_type(PrintStream out, int n) {
        if (type != null)
            { out.println(Utilities.pad(n) + ": " + type.getString()); }
        else
            { out.println(Utilities.pad(n) + ": _no_type"); }
    }
    public abstract void semant(SymbolTable st,ClassTable ct);

}


/** Defines list phylum Expressions
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Expressions extends ListNode {
    public final static Class elementClass = Expression.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Expressions(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Expressions" list */
    public Expressions(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Expression" element to this list */
    public Expressions appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Expressions(lineNumber, copyElements());
    }
}


/** Defines simple phylum Case */
abstract class Case extends TreeNode {
    protected Case(int lineNumber) {
        super(lineNumber);
    }
    public abstract void dump_with_types(PrintStream out, int n);
    public abstract void semant(SymbolTable st,ClassTable ct);

}


/** Defines list phylum Cases
    <p>
    See <a href="ListNode.html">ListNode</a> for full documentation. */
class Cases extends ListNode {
    public final static Class elementClass = Case.class;
    /** Returns class of this lists's elements */
    public Class getElementClass() {
        return elementClass;
    }
    protected Cases(int lineNumber, Vector elements) {
        super(lineNumber, elements);
    }
    /** Creates an empty "Cases" list */
    public Cases(int lineNumber) {
        super(lineNumber);
    }
    /** Appends "Case" element to this list */
    public Cases appendElement(TreeNode elem) {
        addElement(elem);
        return this;
    }
    public TreeNode copy() {
        return new Cases(lineNumber, copyElements());
    }
}


/** Defines AST constructor 'programc'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class programc extends Program {
    protected Classes classes;
    /** Creates "programc" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for classes
      */
    public programc(int lineNumber, Classes a1) {
        super(lineNumber);
        classes = a1;
    }
    public TreeNode copy() {
        return new programc(lineNumber, (Classes)classes.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "programc\n");
        classes.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_program");
        for (Enumeration e = classes.getElements(); e.hasMoreElements(); ) {
            // sm: changed 'n + 1' to 'n + 2' to match changes elsewhere
	    ((Class_)e.nextElement()).dump_with_types(out, n + 2);
        }
    }
    /** This method is the entry point to the semantic checker.  You will
        need to complete it in programming assignment 4.
	<p>
        Your checker should do the following two things:
	<ol>
	<li>Check that the program is semantically correct
	<li>Decorate the abstract syntax tree with type information
        by setting the type field in each Expression node.
        (see tree.h)
	</ol>
	<p>
	You are free to first do (1) and make sure you catch all semantic
    	errors. Part (2) can be done in a second stage when you want
	to test the complete compiler.
    */
    public void semant() {
	    /* ClassTable constructor may do some semantic analysis */
	    ClassTable classTable = new ClassTable(classes);
        SymbolTable st = new SymbolTable();        
        st.enterScope();
        for (Enumeration e = classes.getElements(); e.hasMoreElements(); ) {
            Class_ c = (Class_)e.nextElement();
            c.semant(st,classTable);            
        }
        st.exitScope();

	    if (classTable.errors()) {
	       System.err.println("Compilation halted due to static semantic errors.");
	       System.exit(1);
	    }
    }

}


/** Defines AST constructor 'class_c'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class class_c extends Class_ {
    protected AbstractSymbol name;
    protected AbstractSymbol parent;
    protected Features features;
    protected AbstractSymbol filename;
    /** Creates "class_c" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for parent
      * @param a2 initial value for features
      * @param a3 initial value for filename
      */
    public class_c(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Features a3, AbstractSymbol a4) {
        super(lineNumber);
        name = a1;
        parent = a2;
        features = a3;
        filename = a4;
    }
    public TreeNode copy() {
        return new class_c(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(parent), (Features)features.copy(), copy_AbstractSymbol(filename));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "class_c\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, parent);
        features.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, filename);
    }

    
    public AbstractSymbol getFilename() { return filename; }
    public AbstractSymbol getName()     { return name; }
    public AbstractSymbol getParent() throws NullPointerException  { return parent; }
    public Features getFeatures() { return features; }

    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_class");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, parent);
        out.print(Utilities.pad(n + 2) + "\"");
        Utilities.printEscapedString(out, filename.getString());
        out.println("\"\n" + Utilities.pad(n + 2) + "(");
        for (Enumeration e = features.getElements(); e.hasMoreElements();) {
	    ((Feature)e.nextElement()).dump_with_types(out, n + 2);
        }
        out.println(Utilities.pad(n + 2) + ")");
    }

    public void semant(SymbolTable st,ClassTable ct){
        st.enterScope();
        class_c c_actual = (class_c)copy();
        ct.claseActual(c_actual);

        //-----------------------------------------------------------------------------------------
        //                                  DETECCION DE ERRORES 
        //-----------------------------------------------------------------------------------------

        //Verificamos que no se redefina la clase object
        class_c error1 = ct.checkClasses(c_actual);
        if(c_actual.getName() == TreeConstants.Object_){
            ct.semantError(c_actual).println("Redefinition of basic class Object.");

        //Verificamos que no existan clases repetidas    
        } else if(c_actual.getName() == TreeConstants.Int){
            ct.semantError(c_actual).println("Redefinition of basic class Int.");

        } else if(c_actual.getName() == TreeConstants.SELF_TYPE){
            ct.semantError(c_actual).println("Redefinition of basic class SELF_TYPE.");

        } else if (error1 != null){
            ct.semantError(c_actual).println("Class "+c_actual.getName()+" was previously defined");

        //Verificamos que existe main
        } else if (ct.main_exists() == false){
            ct.semantError().println("Class Main is not defined.");

        //Verificamos que main no herede de SELF_TYPE
        } else if (ct.main_inherits_st() == true){        
            ct.semantError(c_actual).println("Class Main cannot inherit class SELF_TYPE.");

        //Verificamos que ninguna clase (que no sea de las basicas) herede de las clases basicas
        } else if (ct.bad_inherits() != null){
            class_c bad = ct.bad_inherits();
            ct.semantError(c_actual).println("Class "+bad.getName()+" cannot inherit class "+bad.getParent());  

        //Verificamos que ninguna clase herede de otra que no este definida
        } else if (ct.missingClass() != null){
            class_c clase = ct.missingClass();
            ct.semantError(c_actual).println("Class "+clase.getName()+" inherits from an undefined class "+clase.getParent());
        }
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------

        for (Enumeration e = features.getElements(); e.hasMoreElements(); ) {
            ((Feature)e.nextElement()).semant(st,ct);
        }
        st.exitScope();
    }

}


/** Defines AST constructor 'method'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class method extends Feature {
    protected AbstractSymbol name;
    protected Formals formals;
    protected AbstractSymbol return_type;
    protected Expression expr;
    /** Creates "method" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for formals
      * @param a2 initial value for return_type
      * @param a3 initial value for expr
      */
    public method(int lineNumber, AbstractSymbol a1, Formals a2, AbstractSymbol a3, Expression a4) {
        super(lineNumber);
        name = a1;
        formals = a2;
        return_type = a3;
        expr = a4;
    }

    public AbstractSymbol getName() { return name; }
    public Formals getArgs()        { return formals; }
    public AbstractSymbol getType() { return return_type; }

    public TreeNode copy() {
        return new method(lineNumber, copy_AbstractSymbol(name), (Formals)formals.copy(), copy_AbstractSymbol(return_type), (Expression)expr.copy());
    }

    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "method\n");
        dump_AbstractSymbol(out, n+2, name);
        formals.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, return_type);
        expr.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_method");
        dump_AbstractSymbol(out, n + 2, name);
        for (Enumeration e = formals.getElements(); e.hasMoreElements();) {
	    ((Formal)e.nextElement()).dump_with_types(out, n + 2);
        }
        dump_AbstractSymbol(out, n + 2, return_type);
	expr.dump_with_types(out, n + 2);
    }

    public void semant(SymbolTable st,ClassTable ct){
        class_c clase = ct.get_claseActual();
        st.enterScope();
        

        //Verifico si el metodo fue declarado en la clase parent
        class_c parentclass = ct.checkMethodOverride(name,clase);

        //Verifico que no hayan parametros formales repetidos 
        AbstractSymbol var = ct.repeatedArgs(formals);
        if (var != null){
            ct.semantError(clase).println("Formal parameter "+var+" is multiply defined.");
        }

        int cont = 0;
        
        for (Enumeration e = formals.getElements(); e.hasMoreElements(); ) {
            Formal frml = (Formal)e.nextElement();
            frml.semant(st,ct);

            //Si se cumple que el metodo esta declarado en una clase parent, entonces verifico que los argumentos coincidan con esa declaracion
            if (parentclass != null){
                AbstractSymbol errortype = ct.checkArg(parentclass,name,frml,cont);
                if (errortype != null){
                    ct.semantError(clase).println("In redefined method "+name+", parameter type "+((formalc)frml).getTypeDecl()+" is different from original type "+errortype);
                }
            }

            //Verificamos que el parametro del metodo no sea SELF_TYPE
            if (((formalc)frml).getTypeDecl() == TreeConstants.SELF_TYPE){                
                ct.semantError(clase).println("Formal parameter "+((formalc)frml).getName()+" cannot have type SELF_TYPE");
            
            //Verificamos que el nombre del parametro no sea 'self'
            } else if (((formalc)frml).getName() == TreeConstants.self){
                ct.semantError(clase).println("'self' cannot be the name of a formal parameter.");
            }

            st.addId(((formalc)frml).getName(),((formalc)frml).getTypeDecl());

            cont++; 
        }
        expr.semant(st,ct);

        //Verifico que el numero de argumentos del caller sea el mismo que el del calle
        if (ct.checkFormals(name,cont) == false){
            ct.semantError(clase).println("Incompatible number of formal parameters in redefined method "+name+".");
        }

        //Verifico que no haya errores con el return type
        AbstractSymbol t0 = expr.get_type();

        if(return_type == TreeConstants.SELF_TYPE){
            return_type = TreeConstants.SELF_TYPE;
        }

        if (return_type == TreeConstants.SELF_TYPE){
            if (ct.subtype(t0,TreeConstants.SELF_TYPE) == false){
                ct.semantError(clase).println("Inferred return type "+return_type+" of method "+name+" does not conform to declared return type SELF_TYPE.");
            }
        } else if (ct.getClass(return_type) == null){
            ct.semantError(clase).println("Undefined return type "+return_type+" in method "+name);
            ct.semantError(clase).println("'new' used with undefined class "+return_type+".");
        } else if (ct.subtype(t0,return_type) == false && t0 != TreeConstants.SELF_TYPE){
            ct.semantError(clase).println("Inferred return type "+t0+" of method "+name+" does not conform to declared return type "+return_type+".");
        }
        st.exitScope();
    }

}


/** Defines AST constructor 'attr'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class attr extends Feature {
    protected AbstractSymbol name; //x
    protected AbstractSymbol type_decl; //T v To
    protected Expression init; //e1
    /** Creates "attr" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for type_decl
      * @param a2 initial value for init
      */
    public attr(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Expression a3) {
        super(lineNumber);
        name = a1;
        type_decl = a2;
        init = a3;
    }

    public AbstractSymbol getName() { return name; }
    public AbstractSymbol getType() { return type_decl; }

    public TreeNode copy() {
        return new attr(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(type_decl), (Expression)init.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "attr\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, type_decl);
        init.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_attr");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, type_decl);
	init.dump_with_types(out, n + 2);
    }

    public void semant(SymbolTable st,ClassTable ct){
        
        class_c clase = ct.get_claseActual();
        init.semant(st,ct);

        if (name == TreeConstants.self){
            ct.semantError(clase).println("'self' cannot be the name of an attribute.");
        }

        //Attr-Init
        if (init.get_type() != TreeConstants.No_type){
            st.addId(name,type_decl);

        //Attr-No-Init
        } else {
            AbstractSymbol foundid = ct.repeatedAttrs(clase,name);
            if (foundid != null){
                ct.semantError(clase).println("Attribute "+foundid+" is an attribute of an inherited class.");
            } else {
                st.addId(name,type_decl);
            }            
        }        

        //Verificamos que init haya sido declarado
        if(init.get_type() == null){            
            ct.semantError(clase).println("Undeclared identifier "+name+".");
        }
    }

}


/** Defines AST constructor 'formalc'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class formalc extends Formal {
    protected AbstractSymbol name;
    protected AbstractSymbol type_decl;
    /** Creates "formalc" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for type_decl
      */
    public formalc(int lineNumber, AbstractSymbol a1, AbstractSymbol a2) {
        super(lineNumber);
        name = a1;
        type_decl = a2;
    }

    public AbstractSymbol getName()     { return name; }
    public AbstractSymbol getTypeDecl() { return type_decl; }

    public TreeNode copy() {
        return new formalc(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(type_decl));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "formalc\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, type_decl);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_formal");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, type_decl);
    }

    public void semant(SymbolTable st,ClassTable ct){
        st.addId(name,type_decl);
    }

}


/** Defines AST constructor 'branch'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class branch extends Case {
    protected AbstractSymbol name;
    protected AbstractSymbol type_decl;
    protected Expression expr;
    /** Creates "branch" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for type_decl
      * @param a2 initial value for expr
      */
    public branch(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Expression a3) {
        super(lineNumber);
        name = a1;
        type_decl = a2;
        expr = a3;
    }
    public TreeNode copy() {
        return new branch(lineNumber, copy_AbstractSymbol(name), copy_AbstractSymbol(type_decl), (Expression)expr.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "branch\n");
        dump_AbstractSymbol(out, n+2, name);
        dump_AbstractSymbol(out, n+2, type_decl);
        expr.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_branch");
        dump_AbstractSymbol(out, n + 2, name);
        dump_AbstractSymbol(out, n + 2, type_decl);
	expr.dump_with_types(out, n + 2);
    }

    public void semant(SymbolTable st,ClassTable ct){
        expr.semant(st,ct);
    }

}


/** Defines AST constructor 'assign'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class assign extends Expression {
    protected AbstractSymbol name;
    protected Expression expr;
    /** Creates "assign" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      * @param a1 initial value for expr
      */
    public assign(int lineNumber, AbstractSymbol a1, Expression a2) {
        super(lineNumber);
        name = a1;
        expr = a2;
    }
    public TreeNode copy() {
        return new assign(lineNumber, copy_AbstractSymbol(name), (Expression)expr.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "assign\n");
        dump_AbstractSymbol(out, n+2, name);
        expr.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_assign");
        dump_AbstractSymbol(out, n + 2, name);
	expr.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){        
        AbstractSymbol t = (AbstractSymbol)st.lookup(name);
        expr.semant(st,ct);
        AbstractSymbol t1 = expr.get_type();

        boolean condicion = ct.subtype(t1,t);
        if (condicion == true && (name != TreeConstants.self)){
            set_type(t1);    
        } else if (name == TreeConstants.self){
            class_c clase = ct.get_claseActual();
            ct.semantError(clase).println("Cannot assign to 'self'");
        } else if (condicion == false){
            class_c clase = ct.get_claseActual();
            if (t == null){                
                AbstractSymbol t2 = ct.lookForVar(clase,name);
                if (t2 != null){
                    set_type(t2);                    
                } else {
                    ct.semantError(clase).println("Type "+t1+" of assigned expression does not conform to declared type "+t+" of identifier "+name+".");
                }   
            } else {
                ct.semantError(clase).println("Type "+t1+" of assigned expression does not conform to declared type "+t+" of identifier "+name+".");   
            }           
        }      
    }
}


/** Defines AST constructor 'static_dispatch'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class static_dispatch extends Expression {
    protected Expression expr; //e0
    protected AbstractSymbol type_name; //T
    protected AbstractSymbol name; //f
    protected Expressions actual; // arguemntos
    /** Creates "static_dispatch" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for expr
      * @param a1 initial value for type_name
      * @param a2 initial value for name
      * @param a3 initial value for actual
      */
    public static_dispatch(int lineNumber, Expression a1, AbstractSymbol a2, AbstractSymbol a3, Expressions a4) {
        super(lineNumber);
        expr = a1;
        type_name = a2;
        name = a3;
        actual = a4;
    }
    public TreeNode copy() {
        return new static_dispatch(lineNumber, (Expression)expr.copy(), copy_AbstractSymbol(type_name), copy_AbstractSymbol(name), (Expressions)actual.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "static_dispatch\n");
        expr.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, type_name);
        dump_AbstractSymbol(out, n+2, name);
        actual.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_static_dispatch");
	expr.dump_with_types(out, n + 2);
        dump_AbstractSymbol(out, n + 2, type_name);
        dump_AbstractSymbol(out, n + 2, name);
        out.println(Utilities.pad(n + 2) + "(");
        for (Enumeration e = actual.getElements(); e.hasMoreElements();) {
	    ((Expression)e.nextElement()).dump_with_types(out, n + 2);
        }
        out.println(Utilities.pad(n + 2) + ")");
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        expr.semant(st,ct);
        AbstractSymbol t0 = expr.get_type();

        Expression e1 = expr;

        //To <= T
        boolean condicion1 = ct.subtype(t0,type_name);

        // M(T,f) = (T1',...,Tn',Tn+1')
        // Ti <= Ti' 1<=i<=n
        boolean condicion2 = true;
        int cont = 0;
        for (Enumeration e = actual.getElements(); e.hasMoreElements(); ) {
            e1 = (Expression)e.nextElement();
            e1.semant(st,ct);          
            if (condicion2 == true){
                // Ti <= Ti' 1<=i<=n
                condicion2 = ct.M(type_name,name,e1.get_type(),cont);                
            }
            cont++;            
        }
        AbstractSymbol tn = ct.getReturnType(type_name,name);
        if (condicion1 == true && condicion2 == true){
            if (tn == TreeConstants.SELF_TYPE){
                set_type(t0);
            } else {
                set_type(tn);
            }     
        } else if (condicion1 == false){
            class_c c_actual = ct.get_claseActual();
            ct.semantError(c_actual).println("Expression type "+t0+" does not conform to declared static dispatch type "+type_name);
        }
          
    }

}


/** Defines AST constructor 'dispatch'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class dispatch extends Expression {
    protected Expression expr; //e0
    protected AbstractSymbol name; //f
    protected Expressions actual; //argumentos
    /** Creates "dispatch" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for expr
      * @param a1 initial value for name
      * @param a2 initial value for actual
      */
    public dispatch(int lineNumber, Expression a1, AbstractSymbol a2, Expressions a3) {
        super(lineNumber);
        expr = a1;
        name = a2;
        actual = a3;
    }
    public TreeNode copy() {
        return new dispatch(lineNumber, (Expression)expr.copy(), copy_AbstractSymbol(name), (Expressions)actual.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "dispatch\n");
        expr.dump(out, n+2);
        dump_AbstractSymbol(out, n+2, name);
        actual.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_dispatch");
	expr.dump_with_types(out, n + 2);
        dump_AbstractSymbol(out, n + 2, name);
        out.println(Utilities.pad(n + 2) + "(");
        for (Enumeration e = actual.getElements(); e.hasMoreElements();) {
	    ((Expression)e.nextElement()).dump_with_types(out, n + 2);
        }
        out.println(Utilities.pad(n + 2) + ")");
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        expr.semant(st,ct);
        AbstractSymbol t0 = expr.get_type();

        Expression e1 = expr;

        //Definimos To'
        if(t0 == TreeConstants.SELF_TYPE){
            t0 = ct.get_claseActual().getName();
        }

        // M(T,f) = (T1',...,Tn',Tn+1')
        // Ti <= Ti' 1<=i<=n
        boolean condicion1 = true;
        int cont = 0;
        AbstractSymbol arg1 = null;
        AbstractSymbol arg2 = null;
        AbstractSymbol varname = null;
        for (Enumeration e = actual.getElements(); e.hasMoreElements(); ) {
            e1 = (Expression)e.nextElement();
            e1.semant(st,ct);          
            if (condicion1 == true){
                // Ti <= Ti' 1<=i<=n
                condicion1 = ct.M(t0,name,e1.get_type(),cont);
                //Al momento de encontrar un error, obtenemos la informacion necesaria
                if (condicion1 == false){
                    //Antes verificamos que el metodo no haya sido declarado en una de las clases padre
                    if (condicion1 == false){
                        class_c actual = ct.get_claseActual();
                        class_c parent = ct.getClass(actual.getParent());
                        while (parent != null && condicion1 == false){
                            condicion1 = ct.M(parent.getName(),name,e1.get_type(),cont);
                            parent = ct.getClass(parent.getParent());
                        }
                    }
                    arg1 = e1.get_type();
                    arg2 = ct.badArgument();
                    varname = ct.badName();
                }                 
            }
            cont++;            
        }

        if (condicion1 == false){
            class_c clase = ct.get_claseActual();
            ct.semantError(clase).println("In call of method "+name+", type "+arg1+" of parameter "+varname+" does not conform to declared type "+arg2+".");
        }

        AbstractSymbol tn = ct.getReturnType(t0,name);

        if(tn == TreeConstants.No_type){
            class_c clase = ct.get_claseActual();
            ct.semantError(clase).println("Dispatch to undefined method "+name);
        }

        if(tn == TreeConstants.SELF_TYPE){
            set_type(expr.get_type());
        } else {
            set_type(tn);    
        }      
    }
}


/** Defines AST constructor 'cond'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class cond extends Expression {
    protected Expression pred;
    protected Expression then_exp;
    protected Expression else_exp;
    /** Creates "cond" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for pred
      * @param a1 initial value for then_exp
      * @param a2 initial value for else_exp
      */
    public cond(int lineNumber, Expression a1, Expression a2, Expression a3) {
        super(lineNumber);
        pred = a1;
        then_exp = a2;
        else_exp = a3;
    }
    public TreeNode copy() {
        return new cond(lineNumber, (Expression)pred.copy(), (Expression)then_exp.copy(), (Expression)else_exp.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "cond\n");
        pred.dump(out, n+2);
        then_exp.dump(out, n+2);
        else_exp.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_cond");
	pred.dump_with_types(out, n + 2);
	then_exp.dump_with_types(out, n + 2);
	else_exp.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        pred.semant(st,ct);
        then_exp.semant(st,ct);
        else_exp.semant(st,ct);
        AbstractSymbol t1 = pred.get_type();
        AbstractSymbol t2 = then_exp.get_type();
        AbstractSymbol t3 = else_exp.get_type();
        AbstractSymbol lubresult = TreeConstants.No_type;
        if(t1 == TreeConstants.Bool){
            lubresult = ct.lub(t2,t3);
            if (lubresult != TreeConstants.No_type){
                set_type(lubresult);
            } else {
                lubresult = ct.basicLUB(t2,t3);
                set_type(lubresult);
            }            
        }
    }
}


/** Defines AST constructor 'loop'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class loop extends Expression {
    protected Expression pred;
    protected Expression body;
    /** Creates "loop" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for pred
      * @param a1 initial value for body
      */
    public loop(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        pred = a1;
        body = a2;
    }
    public TreeNode copy() {
        return new loop(lineNumber, (Expression)pred.copy(), (Expression)body.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "loop\n");
        pred.dump(out, n+2);
        body.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_loop");
	pred.dump_with_types(out, n + 2);
	body.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        pred.semant(st,ct);
        body.semant(st,ct);
        AbstractSymbol predt = pred.get_type();
        if ((predt == TreeConstants.Bool) && body!=null){
            set_type(TreeConstants.Object_);
        } else if (predt != TreeConstants.Bool){
            class_c clase = ct.get_claseActual();
            ct.semantError(clase).println("Loop condition does not have type Bool.");
        }
    }

}


/** Defines AST constructor 'typcase'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class typcase extends Expression {
    protected Expression expr;
    protected Cases cases;
    /** Creates "typcase" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for expr
      * @param a1 initial value for cases
      */
    public typcase(int lineNumber, Expression a1, Cases a2) {
        super(lineNumber);
        expr = a1;
        cases = a2;
    }
    public TreeNode copy() {
        return new typcase(lineNumber, (Expression)expr.copy(), (Cases)cases.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "typcase\n");
        expr.dump(out, n+2);
        cases.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_typcase");
	expr.dump_with_types(out, n + 2);
        for (Enumeration e = cases.getElements(); e.hasMoreElements();) {
	    ((Case)e.nextElement()).dump_with_types(out, n + 2);
        }
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        expr.semant(st,ct);
        AbstractSymbol leastub = TreeConstants.No_type;
        //Contador que sirve para obtener el primer branch de case
        int i = 0;

        st.enterScope();
        for (Enumeration e = cases.getElements(); e.hasMoreElements(); ) {
            Case c = (Case)e.nextElement();
            branch b = (branch)c;
            st.addId(b.name,b.type_decl);
            c.semant(st,ct); 
            //Verificamos que todos los branches tengan distinto tipo
            boolean condicion = ct.checkCases(b.type_decl);
            if (condicion == false){
                class_c clase = ct.get_claseActual();
                ct.semantError(clase).println("Duplicate branch "+b.type_decl+" in case statement.");
            }

                     
            if (i == 0){
                leastub = (b.expr).get_type();
            } else {
                leastub = ct.lub((b.expr).get_type(),leastub);
                if(leastub == TreeConstants.No_type){
                    leastub = ct.basicLUB((b.expr).get_type(),leastub);
                }
            }
            i++;
        }
        ct.clearCases();
        st.exitScope();
        set_type(leastub);
        
    }

}


/** Defines AST constructor 'block'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class block extends Expression {
    protected Expressions body;
    /** Creates "block" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for body
      */
    public block(int lineNumber, Expressions a1) {
        super(lineNumber);
        body = a1;
    }
    public TreeNode copy() {
        return new block(lineNumber, (Expressions)body.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "block\n");
        body.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_block");
        for (Enumeration e = body.getElements(); e.hasMoreElements();) {
	    ((Expression)e.nextElement()).dump_with_types(out, n + 2);
        }
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){

        for (Enumeration e = body.getElements(); e.hasMoreElements(); ) {
            Expression e1 = (Expression)e.nextElement();
            (e1).semant(st,ct);
            set_type(e1.get_type());
        }
    }

}


/** Defines AST constructor 'let'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class let extends Expression {
    protected AbstractSymbol identifier; //x
    protected AbstractSymbol type_decl; //To
    protected Expression init; 
    protected Expression body;
    /** Creates "let" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for identifier
      * @param a1 initial value for type_decl
      * @param a2 initial value for init
      * @param a3 initial value for body
      */
    public let(int lineNumber, AbstractSymbol a1, AbstractSymbol a2, Expression a3, Expression a4) {
        super(lineNumber);
        identifier = a1;
        type_decl = a2;
        init = a3;
        body = a4;
    }
    public TreeNode copy() {
        return new let(lineNumber, copy_AbstractSymbol(identifier), copy_AbstractSymbol(type_decl), (Expression)init.copy(), (Expression)body.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "let\n");
        dump_AbstractSymbol(out, n+2, identifier);
        dump_AbstractSymbol(out, n+2, type_decl);
        init.dump(out, n+2);
        body.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_let");
	dump_AbstractSymbol(out, n + 2, identifier);
	dump_AbstractSymbol(out, n + 2, type_decl);
	init.dump_with_types(out, n + 2);
	body.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){ 
        st.enterScope();
        AbstractSymbol t0;
        if (type_decl == TreeConstants.SELF_TYPE){
            t0 = TreeConstants.SELF_TYPE;
        } else {
            t0 = type_decl;
        }

        st.addId(identifier,t0);
        init.semant(st,ct);        
        body.semant(st,ct);
        AbstractSymbol t1 = init.get_type();
        //Let-Init
        if (t1 != TreeConstants.No_type){
            if (ct.subtype(t1,t0) == true){
                set_type(body.get_type());
            } else {
                class_c clase = ct.get_claseActual();
                ct.semantError(clase).println("Inferred type "+t1+" of initialization of x does not conform to identifier's declared type "+t0);
            } 
        //Let-No-Init
        } else {
            if(identifier == TreeConstants.self){
                class_c clase = ct.get_claseActual();
                ct.semantError(clase).println("'self' cannot be bound in a 'let' expression.");
            }
            set_type(body.get_type());
        }        

        st.exitScope();
    }

}


/** Defines AST constructor 'plus'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class plus extends Expression {
    protected Expression e1;
    protected Expression e2;
    /** Creates "plus" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public plus(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;        
    }
    public TreeNode copy() {
        return new plus(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "plus\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_plus");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        e2.semant(st,ct);
        AbstractSymbol e1t = e1.get_type();
        AbstractSymbol e2t = e2.get_type();

        if ((e1t == e2t) && (e1t == TreeConstants.Int)) {
            set_type(TreeConstants.Int);                   
        } else {
            class_c clase = ct.get_claseActual();
            ct.semantError(clase).println("non-Int arguments: "+e1t+" + "+e2t);
        } 
    }
}


/** Defines AST constructor 'sub'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class sub extends Expression {
    protected Expression e1;
    protected Expression e2;
    /** Creates "sub" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public sub(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new sub(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "sub\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_sub");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        e2.semant(st,ct);
        AbstractSymbol e1t = e1.get_type();
        AbstractSymbol e2t = e2.get_type();

        if ((e1t == e2t) && (e1t == TreeConstants.Int)) {
            set_type(TreeConstants.Int);                   
        } else {
            class_c clase = ct.get_claseActual();
            ct.semantError(clase).println("non-Int arguments: "+e1t+" + "+e2t);
        }
    }

}


/** Defines AST constructor 'mul'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class mul extends Expression {
    protected Expression e1;
    protected Expression e2;
    /** Creates "mul" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public mul(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new mul(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "mul\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_mul");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        e2.semant(st,ct);
        AbstractSymbol e1t = e1.get_type();
        AbstractSymbol e2t = e2.get_type();

        if ((e1t == e2t) && (e1t == TreeConstants.Int)) {
            set_type(TreeConstants.Int);                   
        }
    }

}


/** Defines AST constructor 'divide'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class divide extends Expression {
    protected Expression e1;
    protected Expression e2;
    /** Creates "divide" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public divide(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new divide(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "divide\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_divide");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        e2.semant(st,ct);
        AbstractSymbol e1t = e1.get_type();
        AbstractSymbol e2t = e2.get_type();

        if ((e1t == e2t) && (e1t == TreeConstants.Int)) {
            set_type(TreeConstants.Int);                   
        } 
    }

}


/** Defines AST constructor 'neg'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class neg extends Expression {
    protected Expression e1;
    /** Creates "neg" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      */
    public neg(int lineNumber, Expression a1) {
        super(lineNumber);
        e1 = a1;
    }
    public TreeNode copy() {
        return new neg(lineNumber, (Expression)e1.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "neg\n");
        e1.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_neg");
	e1.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        if (e1.get_type() == TreeConstants.Int){
            set_type(TreeConstants.Int);
        } 
    }

}


/** Defines AST constructor 'lt'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class lt extends Expression {
    protected Expression e1;
    protected Expression e2;
    /** Creates "lt" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public lt(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new lt(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "lt\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_lt");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        e2.semant(st,ct);
        AbstractSymbol e1t = e1.get_type();
        AbstractSymbol e2t = e2.get_type();

        if ((e1t == e2t) && (e1t == TreeConstants.Int)){
            set_type(TreeConstants.Bool);
        } 
    }

}


/** Defines AST constructor 'eq'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class eq extends Expression {
    protected Expression e1;
    protected Expression e2;
    /** Creates "eq" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public eq(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new eq(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "eq\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_eq");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        e2.semant(st,ct);
        AbstractSymbol e1t = e1.get_type();
        AbstractSymbol e2t = e2.get_type();

        if (e1t == TreeConstants.Int || e1t == TreeConstants.Str || e1t == TreeConstants.Bool || e2t == TreeConstants.Int || e2t == TreeConstants.Str || e2t == TreeConstants.Bool) {
            if (e1t != e2t){
                class_c clase = ct.get_claseActual();
                ct.semantError(clase).println("Illegal comparison with a basic type.");
            }
        }
        if (e1t != null && e2t != null){
           set_type(TreeConstants.Bool); 
        }
         
    }

}


/** Defines AST constructor 'leq'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class leq extends Expression {
    protected Expression e1;
    protected Expression e2;
    /** Creates "leq" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      * @param a1 initial value for e2
      */
    public leq(int lineNumber, Expression a1, Expression a2) {
        super(lineNumber);
        e1 = a1;
        e2 = a2;
    }
    public TreeNode copy() {
        return new leq(lineNumber, (Expression)e1.copy(), (Expression)e2.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "leq\n");
        e1.dump(out, n+2);
        e2.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_leq");
	e1.dump_with_types(out, n + 2);
	e2.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        e2.semant(st,ct);
        AbstractSymbol e1t = e1.get_type();
        AbstractSymbol e2t = e2.get_type();

        if ((e1t == e2t) && (e1t == TreeConstants.Int)){
            set_type(TreeConstants.Bool);
        } 
    }
}


/** Defines AST constructor 'comp'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class comp extends Expression {
    protected Expression e1;
    /** Creates "comp" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      */
    public comp(int lineNumber, Expression a1) {
        super(lineNumber);
        e1 = a1;
    }
    public TreeNode copy() {
        return new comp(lineNumber, (Expression)e1.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "comp\n");
        e1.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_comp");
	e1.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        if(e1.get_type() == TreeConstants.Bool){
            set_type(TreeConstants.Bool);
        }
    }

}


/** Defines AST constructor 'int_const'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class int_const extends Expression {
    protected AbstractSymbol token;
    /** Creates "int_const" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for token
      */
    public int_const(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        token = a1;
    }
    public TreeNode copy() {
        return new int_const(lineNumber, copy_AbstractSymbol(token));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "int_const\n");
        dump_AbstractSymbol(out, n+2, token);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_int");
	dump_AbstractSymbol(out, n + 2, token);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        set_type(TreeConstants.Int);
    }

}


/** Defines AST constructor 'bool_const'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class bool_const extends Expression {
    protected Boolean val;
    /** Creates "bool_const" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for val
      */
    public bool_const(int lineNumber, Boolean a1) {
        super(lineNumber);
        val = a1;
    }
    public TreeNode copy() {
        return new bool_const(lineNumber, copy_Boolean(val));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "bool_const\n");
        dump_Boolean(out, n+2, val);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_bool");
	dump_Boolean(out, n + 2, val);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        set_type(TreeConstants.Bool);
    }

}


/** Defines AST constructor 'string_const'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class string_const extends Expression {
    protected AbstractSymbol token;
    /** Creates "string_const" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for token
      */
    public string_const(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        token = a1;
    }
    public TreeNode copy() {
        return new string_const(lineNumber, copy_AbstractSymbol(token));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "string_const\n");
        dump_AbstractSymbol(out, n+2, token);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_string");
	out.print(Utilities.pad(n + 2) + "\"");
	Utilities.printEscapedString(out, token.getString());
	out.println("\"");
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        set_type(TreeConstants.Str);
    }

}


/** Defines AST constructor 'new_'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class new_ extends Expression {
    protected AbstractSymbol type_name;
    /** Creates "new_" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for type_name
      */
    public new_(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        type_name = a1;
    }
    public TreeNode copy() {
        return new new_(lineNumber, copy_AbstractSymbol(type_name));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "new_\n");
        dump_AbstractSymbol(out, n+2, type_name);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_new");
	dump_AbstractSymbol(out, n + 2, type_name);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        if (type_name == TreeConstants.SELF_TYPE){
            set_type(TreeConstants.SELF_TYPE);
        } else {
            set_type(type_name);    
        }
        
    }

}


/** Defines AST constructor 'isvoid'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class isvoid extends Expression {
    protected Expression e1;
    /** Creates "isvoid" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for e1
      */
    public isvoid(int lineNumber, Expression a1) {
        super(lineNumber);
        e1 = a1;
    }
    public TreeNode copy() {
        return new isvoid(lineNumber, (Expression)e1.copy());
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "isvoid\n");
        e1.dump(out, n+2);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_isvoid");
	e1.dump_with_types(out, n + 2);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        e1.semant(st,ct);
        if (e1 != null){
            set_type(TreeConstants.Bool);
        }        
    }

}


/** Defines AST constructor 'no_expr'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class no_expr extends Expression {
    /** Creates "no_expr" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      */
    public no_expr(int lineNumber) {
        super(lineNumber);
    }
    public TreeNode copy() {
        return new no_expr(lineNumber);
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "no_expr\n");
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_no_expr");
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        set_type(TreeConstants.No_type);
    }

}


/** Defines AST constructor 'object'.
    <p>
    See <a href="TreeNode.html">TreeNode</a> for full documentation. */
class object extends Expression {
    protected AbstractSymbol name;
    /** Creates "object" AST node. 
      *
      * @param lineNumber the line in the source file from which this node came.
      * @param a0 initial value for name
      */
    public object(int lineNumber, AbstractSymbol a1) {
        super(lineNumber);
        name = a1;
    }
    public TreeNode copy() {
        return new object(lineNumber, copy_AbstractSymbol(name));
    }
    public void dump(PrintStream out, int n) {
        out.print(Utilities.pad(n) + "object\n");
        dump_AbstractSymbol(out, n+2, name);
    }

    
    public void dump_with_types(PrintStream out, int n) {
        dump_line(out, n);
        out.println(Utilities.pad(n) + "_object");
	dump_AbstractSymbol(out, n + 2, name);
	dump_type(out, n);
    }

    public void semant(SymbolTable st,ClassTable ct){
        if (name == TreeConstants.self){
            set_type(TreeConstants.SELF_TYPE);
        } else {
            AbstractSymbol t = (AbstractSymbol)st.lookup(name);
            if (t != null){
                set_type(t);
            } else {
                class_c clase = ct.get_claseActual();
                //Si no encuentra la variable en el scope actual, va a buscar a las clases padre 
                AbstractSymbol t1 = ct.lookForVar(clase,name);
                if (t1 != null){
                    set_type(t1);
                } else {
                    ct.semantError(clase).println("Undeclared identifier "+name+".");
                }               
            }
        }
    }

}



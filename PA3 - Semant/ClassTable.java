import java.io.PrintStream;
import java.util.*;

/** This class may be used to contain the semantic information such as
 * the inheritance graph.  You may use it or not as you like: it is only
 * here to provide a container for the supplied methods.  */
class ClassTable {
    private int semantErrors;
    private PrintStream errorStream;
    //Creamos un arraylist para almacenar las clases
    private ArrayList<class_c> tabla = new ArrayList();
    //Arraylist que me ayuda a detectar errores en case
    private ArrayList<AbstractSymbol> types = new ArrayList();
    //Arraylist de class_c para detectar que no haya clases repetidas
    private ArrayList<AbstractSymbol> repeatedclass = new ArrayList();
    //Utilizamos esta variable para saber la clase actual
    private class_c claseactual = null;
    //Variables que nos ayudan con la deteccion de errores
    private class_c clase_error = null, clase_error1 = null;
    //Utilizamos estas variables para deolver informacion acerca de los errores 
    private AbstractSymbol badType = null, badName = null;
    //Variables auxiliares que ayudan a que la deteccion de errores se realice una sola vez
    private int times = 0, howmany = 0, times1 = 0;

    /** Creates data structures representing basic Cool classes (Object,
     * IO, Int, Bool, String).  Please note: as is this method does not
     * do anything useful; you will need to edit it to make if do what
     * you want.
     * */
    private void installBasicClasses() {
	AbstractSymbol filename 
	    = AbstractTable.stringtable.addString("<basic class>");
	
	// The following demonstrates how to create dummy parse trees to
	// refer to basic Cool classes.  There's no need for method
	// bodies -- these are already built into the runtime system.

	// IMPORTANT: The results of the following expressions are
	// stored in local variables.  You will want to do something
	// with those variables at the end of this method to make this
	// code meaningful.

	// The Object class has no parent class. Its methods are
	//        cool_abort() : Object    aborts the program
	//        type_name() : Str        returns a string representation 
	//                                 of class name
	//        copy() : SELF_TYPE       returns a copy of the object

	class_c Object_class = 
	    new class_c(0, 
		       TreeConstants.Object_, 
		       TreeConstants.No_class,new Features(0)	.appendElement(new method(0,TreeConstants.cool_abort,new Formals(0),TreeConstants.Object_,new no_expr(0)))
			   											.appendElement(new method(0,TreeConstants.type_name,new Formals(0),TreeConstants.Str,new no_expr(0)))
			   											.appendElement(new method(0,TreeConstants.copy,new Formals(0),TreeConstants.SELF_TYPE,new no_expr(0))), filename);
	
	// The IO class inherits from Object. Its methods are
	//        out_string(Str) : SELF_TYPE  writes a string to the output
	//        out_int(Int) : SELF_TYPE      "    an int    "  "     "
	//        in_string() : Str            reads a string from the input
	//        in_int() : Int                "   an int     "  "     "

	class_c IO_class = 
	    new class_c(0,
		       TreeConstants.IO,
		       TreeConstants.Object_,
		       new Features(0)	.appendElement(new method(0,	TreeConstants.out_string,new Formals(0)
						  		.appendElement(new formalc(0,	TreeConstants.arg,TreeConstants.Str)),TreeConstants.SELF_TYPE,new no_expr(0)))
			   					.appendElement(new method(0,	TreeConstants.out_int,new Formals(0)
						  		.appendElement(new formalc(0,	TreeConstants.arg,TreeConstants.Int)),TreeConstants.SELF_TYPE,new no_expr(0)))
			   					.appendElement(new method(0,	TreeConstants.in_string,new Formals(0),TreeConstants.Str,new no_expr(0)))
			   					.appendElement(new method(0,	TreeConstants.in_int,new Formals(0),TreeConstants.Int,new no_expr(0))),		       filename);

	// The Int class has no methods and only a single attribute, the
	// "val" for the integer.

	class_c Int_class = 
	    new class_c(0,
		       TreeConstants.Int,
		       TreeConstants.Object_,
		       new Features(0).appendElement(new attr(0,TreeConstants.val,TreeConstants.prim_slot,new no_expr(0))),
		       filename);

	// Bool also has only the "val" slot.
	class_c Bool_class = 
	    new class_c(0,
		       TreeConstants.Bool,
		       TreeConstants.Object_,
		       new Features(0)   .appendElement(new attr(0,TreeConstants.val,TreeConstants.prim_slot,new no_expr(0))),
		       filename);

	// The class Str has a number of slots and operations:
	//       val                              the length of the string
	//       str_field                        the string itself
	//       length() : Int                   returns length of the string
	//       concat(arg: Str) : Str           performs string concatenation
	//       substr(arg: Int, arg2: Int): Str substring selection

	class_c Str_class =
	    new class_c(0,
		       TreeConstants.Str,
		       TreeConstants.Object_,
		       new Features(0)	.appendElement(new attr(0,		TreeConstants.val,TreeConstants.Int,new no_expr(0)))
			   					.appendElement(new attr(0,		TreeConstants.str_field,TreeConstants.prim_slot,new no_expr(0)))
			   					.appendElement(new method(0,	TreeConstants.length,new Formals(0),TreeConstants.Int,new no_expr(0)))
			   					.appendElement(new method(0,	TreeConstants.concat,new Formals(0)
						  		.appendElement(new formalc(0,	TreeConstants.arg, TreeConstants.Str)),TreeConstants.Str,new no_expr(0)))
			   					.appendElement(new method(0,	TreeConstants.substr,new Formals(0)
						  		.appendElement(new formalc(0,	TreeConstants.arg,TreeConstants.Int))
						  		.appendElement(new formalc(0,	TreeConstants.arg2,TreeConstants.Int)),TreeConstants.Str,new no_expr(0))),filename);

	/* Do somethind with Object_class, IO_class, Int_class,
           Bool_class, and Str_class here */
    	//agregamos las clases basicas a la tabla 
           tabla.add(Object_class);
           tabla.add(IO_class);
           tabla.add(Int_class);
           tabla.add(Bool_class);
           tabla.add(Str_class);

    }
	
    private void addOtherClasses(Classes cls){
        for(int i=0;i<cls.getLength();i++){
            class_c clase = (class_c)cls.getNth(i);
            tabla.add(clase);
        }
    }


    public ClassTable(Classes cls) {
		semantErrors = 0;
		errorStream = System.err;
	
		//Llamamos al metodo InstallBasicClasses para agregar las clases Object_class, IO_class, Int_class, Bool_class y Str_class a la tabla
		installBasicClasses();
		//Agregamos las demas clases (que vienen en cls) a la tabla 
        addOtherClasses(cls);

        //Obtenemos el resultado de la verificacion de inherits_from_basic()
        clase_error = inherits_from_basic();
        checkParents();
		
    }

    //Guardamos la clase actual en una variable
   	public void claseActual(class_c clase){
   		claseactual = clase;
   	}
   	//Metodo que devuelve la clase actual
   	public class_c get_claseActual(){
   		return claseactual;
   	}

    //----------------------------------------------
    //             DETECCION DE ERRORES
    //----------------------------------------------
    //Verificamos que exista main
    public boolean main_exists(){
        for(int i=0;i<tabla.size();i++){
            if (tabla.get(i).getName() == TreeConstants.Main){
                return true;
            }
        }
        return false;
    }

    //Verificamos que Main no herede de SELF_TYPE
    public boolean main_inherits_st(){
        for(int i=0;i<tabla.size();i++){
            class_c main = tabla.get(i);
            if (main.getName() == TreeConstants.Main){
                if (main.getParent() == TreeConstants.SELF_TYPE){
                    return true;
                }
            }
        }
        return false;
    }

    //Verificamos que las clases que no son basicas no hereden de las mismas
    public class_c inherits_from_basic(){
        class_c clase = null;
        for(int i=0;i<tabla.size();i++){
            clase = tabla.get(i);
            AbstractSymbol nombre = clase.getName();        
            if(nombre != TreeConstants.Main && nombre != TreeConstants.Object_ && nombre != TreeConstants.IO && nombre != TreeConstants.Int && nombre != TreeConstants.Bool && nombre != TreeConstants.Str){
                AbstractSymbol parent = clase.getParent();        
                if (parent == TreeConstants.Int || parent == TreeConstants.Bool || parent == TreeConstants.Str){
                    break;
                }
            }
            clase = null;
        }
        return clase;
    }
    public class_c bad_inherits(){        
        return clase_error;
    }

    //Verificamos que ninguna clase herede de otra no definida
    public void checkParents(){
        for (int i=0;i<tabla.size();i++){
            clase_error1 = tabla.get(i);
            AbstractSymbol parent = clase_error1.getParent();            
            if  ((getClass(parent) == null) && (clase_error1.getName() != TreeConstants.Object_)){
                howmany++;
                break;
            }
        }
        if(howmany == 0){
            clase_error1 = null;
        }
    }
    public class_c missingClass(){
        if (times <= 1){
            times++;
            return clase_error1;
        } else {
            class_c ret = null;
            return ret;
        }        
    }

    //Verificamos que en una lista de cases solo aparezca un maximo una vez un type
    public boolean checkCases(AbstractSymbol type){        
        if (types.indexOf(type) == -1){
            types.add(type);
            return true;
        } else {
            return false;
        }
    }

    public void clearCases(){
        types.clear();
    }

    //Verificamos que no existan clases repetidas
    public class_c checkClasses(class_c c){
        if (repeatedclass.indexOf(c.getName()) == -1){
            repeatedclass.add(c.getName());
            return null;
        } else {
            return c;
        } 
    }

    //Verificamos que el numero de argumentos del caller sean los mismos que el del callee
    public boolean checkFormals(AbstractSymbol nombrefuncion, int n){
        //Obtengo clase por clase de la tabla
        for(int i=0;i<tabla.size();i++){            
            class_c clase = tabla.get(i);
            Features flist = clase.getFeatures();
            //Obtengo los features de la clase
            for (Enumeration e=flist.getElements();e.hasMoreElements();){
                Feature ftr = (Feature)e.nextElement();                  
                if (ftr instanceof method){
                    AbstractSymbol nombre = ((method)ftr).getName();
                    //Verifico que el feature es el que busco
                    if (nombre == nombrefuncion){
                        //Obtengo los argumentos de ese metodo
                        Formals argslist = ((method)ftr).getArgs();
                        int cont = 0;
                        for (Enumeration e1=argslist.getElements();e1.hasMoreElements(); ){
                            //Sumo +1 al contador por cada arguemto
                            Formal f = (Formal)e1.nextElement();
                            cont++;
                        }
                        //si la cantidad de argumentos es igual devuelve true, de lo contrario, false
                        if (cont == n){
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    //Verifica que cada argumento sea subtipo del argumento formal del metodo

    //Verifico que un metodo de una subclase no tenga mal implementado el metodo de la clase padre
    public class_c checkMethodOverride(AbstractSymbol nombre, class_c claseactual){
        //Obtenemos la clase parent de la clase actual
        class_c parent = getClass(claseactual.getParent());
        //Obtenemos la lista de features de esa clase
        if(parent==null) return null;
        Features flist = parent.getFeatures();
        for (Enumeration e=flist.getElements();e.hasMoreElements();){
            //Obtengo cada feature (metodo)
            Feature ftr = (Feature)e.nextElement();                  
            if (ftr instanceof method){
                AbstractSymbol nombrefuncion = ((method)ftr).getName();
                //Verifico si el metodo es el que busco
                if (nombrefuncion == nombre){
                    return parent;                   
                }                    
            }          
        }
        return null;
    }
    //Este metodo complementa al checkMethodOverride cuando este devuelve algo distinto de null
    //Verifico que cada argumento coincida con el de la clase parent
    public AbstractSymbol checkArg(class_c parent, AbstractSymbol nombre, Formal f, int indice){
        //Obtengo la lista de features 
        Features flist = parent.getFeatures();
        for (Enumeration e=flist.getElements();e.hasMoreElements();){
            //Obtengo cada feature (metodo)
            Feature ftr = (Feature)e.nextElement();
            if (ftr instanceof method){
                AbstractSymbol nombrefuncion = ((method)ftr).getName();
                //Verifico si el metodo es el que busco
                if (nombrefuncion == nombre){
                    //Obtengo los argumentos del metodo
                    Formals argslist = ((method)ftr).getArgs();
                    int cont = 0;
                    for (Enumeration e1=argslist.getElements();e1.hasMoreElements();){
                        Formal arg = (Formal)e1.nextElement();
                        //Obtengo el type con el que fue declarado el metodo
                        AbstractSymbol n = ((formalc)arg).getTypeDecl();
                        //Verifico que el tipo del caller sea subtipo del que esta declarado
                        AbstractSymbol t = ((formalc)f).getTypeDecl();
                        if ((t != n) && cont == indice){                            
                            return n;
                        } 
                        cont++;  
                    }                    
                }                    
            }  
        }
        return null;
    }

    //Metodo para verificar si hay o no, parametros repetidos en un metodo
    public AbstractSymbol repeatedArgs(Formals formals){
        ArrayList<AbstractSymbol> lista = new ArrayList();
        if (formals != null){
            for (Enumeration e1=formals.getElements();e1.hasMoreElements();){
                Formal arg = (Formal)e1.nextElement();
                AbstractSymbol var = ((formalc)arg).getName();
                if (lista.indexOf(var) == -1){
                    lista.add(var);
                } else {
                    return var;
                }
            }
        }
        return null;
    }

    public AbstractSymbol repeatedAttrs(class_c claseactual, AbstractSymbol id){
        class_c parent = getClass(claseactual.getParent());
        if(parent == null || times1 != 0) return null;
        //Obtengo la lista de features 
        Features flist = parent.getFeatures();
        for (Enumeration e=flist.getElements();e.hasMoreElements();){
            //Obtengo cada feature (ahora busco attr's)
            Feature ftr = (Feature)e.nextElement();
            if (ftr instanceof attr){
                AbstractSymbol idname = ((attr)ftr).getName();
                if (idname == id){
                    times1++;
                    return idname;
                }
            }
        }
        return repeatedAttrs(parent,id);
    }

    //metodo que va a buscar la variable a sus clases padre
    public AbstractSymbol lookForVar(class_c claseactual, AbstractSymbol var){
        if (claseactual == null) return null;
        //Obtengo la lista de features
        Features flist = claseactual.getFeatures();
        for (Enumeration e=flist.getElements();e.hasMoreElements();){
            //Obtengo cada feature (ahora busco attr's)
            Feature ftr = (Feature)e.nextElement();
            if (ftr instanceof attr){
                AbstractSymbol varname = ((attr)ftr).getName();
                if (var == varname){
                    return ((attr)ftr).getType();
                }
            }
        }
        return lookForVar(getClass(claseactual.getParent()),var);
    }

    //----------------------------------------------------------------------------
    //----------------------------------------------------------------------------

    //Metodo para obtener el least upper boud de 2 types
    public AbstractSymbol lub(AbstractSymbol t1, AbstractSymbol t2){ 
        boolean cond1 = subtype(t1,t2);
        boolean cond2 = subtype(t2,t1);
        if(cond1 == true && cond2 == false && t1 != TreeConstants.Object_ && t2 != TreeConstants.Object_){
            return t2;
        } else if (cond2 == true && cond1 == false &&t1 != TreeConstants.Object_ && t2 != TreeConstants.Object_){
            return t1;
        }
        if (t1 == t2){         
            return t1;
        } else {
            class_c clase_t1 = getClass(t1);
            class_c clase_t2 = getClass(t2);
            if(clase_t1 == null || clase_t2 == null) return TreeConstants.No_type;
            AbstractSymbol parent1 = clase_t1.getParent();
            AbstractSymbol parent2 = clase_t2.getParent();
            
            if (t1 == TreeConstants.Object_){
                return TreeConstants.No_type;
            } else if(t2 == TreeConstants.Object_){
                return TreeConstants.No_type;
            } else {
                return lub(parent1,parent2); 
            }            
        }
    }

    //metodo para obtener el least upper bound entre 2 clases basicas (Chapuz horrible XD)
    public AbstractSymbol basicLUB(AbstractSymbol t1, AbstractSymbol t2){
        if (t1 == TreeConstants.Int || t1 == TreeConstants.Str || t1 == TreeConstants.Bool || t1 == TreeConstants.SELF_TYPE || t2 == TreeConstants.Int || t2 == TreeConstants.Str || t2 == TreeConstants.Bool || t2 == TreeConstants.SELF_TYPE){
            return TreeConstants.Object_;
        } else {
            return TreeConstants.No_type;
        }
    }

    //metodo para verificar si un tipo t1 es subtype de otro tipo t2
    public boolean subtype(AbstractSymbol t1, AbstractSymbol t2){
        if (t1 == t2){
            return true;
        } else {
            class_c clase_t1 = getClass(t1);
            if(clase_t1 == null) return false;
            AbstractSymbol parent = clase_t1.getParent();
            boolean ret = subtype(parent,t2);
            return ret;
        }
    }

    public boolean subtype1(AbstractSymbol t1, AbstractSymbol t2){
        if (t1 == t2){
            return true;
        } else {            
            while (t1 != t2 && t1 != TreeConstants.No_class){
                t1 = (getClass(t1)).getParent();
            }
            if (t1 == t2) {
                return true;
            } else {
                return false;
            }
        }
    }

    //metodo para el static_dispatch y dispatch
    public boolean M(AbstractSymbol t, AbstractSymbol f, AbstractSymbol type, int indice){
        //Obtengo la clase T
        class_c clase = getClass(t);
        if(clase != null){
            //Obtengo los metodos de la clase T
            Features flist = clase.getFeatures();
            for (Enumeration e=flist.getElements();e.hasMoreElements();){
                Feature ftr = (Feature)e.nextElement();                  
                if (ftr instanceof attr){
                    return true;
                } else if (ftr instanceof method){
                    AbstractSymbol nombrefuncion = ((method)ftr).getName();
                    //Verifico si el metodo es el que busco                    
                    if (nombrefuncion == f){
                        //Obtengo los argumentos del metodo
                        Formals argslist = ((method)ftr).getArgs();
                        int cont = 0;
                        for (Enumeration e1=argslist.getElements();e1.hasMoreElements();){
                            Formal arg = (Formal)e1.nextElement();
                            AbstractSymbol n = ((formalc)arg).getTypeDecl();
                            AbstractSymbol n1 = ((formalc)arg).getName();                         
                            //Verifico que el tipo del caller sea subtipo del que esta declarado
                            if (subtype(type,n) == true && (cont == indice)){
                                return true;
                            //Si no se cumple la condicion entonces devolvemos el error con badArgument
                            } else if (subtype(type,n) == false && cont == indice){
                                badType = n;
                                badName = n1;
                                return false;
                            }   
                            cont++;
                        }                        
                        return true;
                    }                    
                }          
            }
        }
        return false;
    }

    //Metodo auxiliar del metodo M que devolvera informacion util para la deteccion de errores
    public AbstractSymbol badArgument() { return badType; }
    public AbstractSymbol badName() { return badName; }

    //Metodo para obtener el Tn+1'
    public AbstractSymbol getReturnType(AbstractSymbol c, AbstractSymbol f){
        //Primero buscamos en la clase Object_class para descartarla
        for(int i=0;i<tabla.size();i++){            
            class_c obj = tabla.get(i);
            //Obtenemos la clase Object_class
            if (obj.getName() == TreeConstants.Object_){
                //Obtenemos la lista de features (metodos) de la clase object
                Features flist1 = obj.getFeatures();                
                for (Enumeration e = flist1.getElements(); e.hasMoreElements(); ) {                    
                    Feature ftr = (Feature)e.nextElement();                    
                    if (ftr instanceof method){
                        AbstractSymbol name = ((method)ftr).getName();
                        if (name == f){
                            return((method)ftr).getType();
                        }
                    }
                }
            }
        }
        //Ahora buscamos en las otras clases
            class_c otherclass = getClass(c);
            //Obtenemos el nombre de la clase
            if(otherclass != null){                            
                //Obtenemos la lista de features de la clase
                Features flist2 = otherclass.getFeatures();
                for (Enumeration e = flist2.getElements(); e.hasMoreElements(); ) {
                    //Para cada feature, verificamos si es Feature (solo por precaucion :P)
                    Object o = e.nextElement();
                    if (o instanceof Feature){
                        Feature ftr = (Feature)o;
                        //Verificamos si el feature es attr o method 
                        if (ftr instanceof attr){
                            AbstractSymbol name = ((attr)ftr).getName();                            
                            //Comparamos el nombre(argumento) con el nombre de la funcion
                            if (name == f){
                                return ((attr)ftr).getType();
                            }
                        } else if (ftr instanceof method){
                            AbstractSymbol name = ((method)ftr).getName();
                            //Comparamos el nombre(argumento) con el nombre de la funcion
                            if (name == f){
                                return ((method)ftr).getType();
                            }
                        } 
                    }
                }
            }
        //Si no encuentra el metodo en ninguno de los metodos de la clase, buscara en el parent de la clase
            AbstractSymbol parent_name = otherclass.getParent();
            class_c parent = getClass(parent_name);
            if (parent == null) return TreeConstants.No_type;
            AbstractSymbol result = getReturnType(parent_name,f);
            return result;                
    }

    //metodo para devolver la clase
    public class_c getClass(AbstractSymbol c){
    	for(int i=0;i<tabla.size();i++){
    		//Obtenemos una clase de la tabla
    		class_c temp_class = tabla.get(i);            
    		//Obtenemos el nombre de esa clase
    		AbstractSymbol name = temp_class.getName();
    		//Verificamos si el nombre de la clase es el que estamos buscando            
    		if (name == c){
    			return temp_class;
    		}
    	}
    	return null;
    }



    /** Prints line number and file name of the given class.
     *
     * Also increments semantic error count.
     *
     * @param c the class
     * @return a print stream to which the rest of the error message is
     * to be printed.
     *
     * */
    public PrintStream semantError(class_c c) {
	return semantError(c.getFilename(), c);
    }

    /** Prints the file name and the line number of the given tree node.
     *
     * Also increments semantic error count.
     *
     * @param filename the file name
     * @param t the tree node
     * @return a print stream to which the rest of the error message is
     * to be printed.
     *
     * */
    public PrintStream semantError(AbstractSymbol filename, TreeNode t) {
	errorStream.print(filename + ":" + t.getLineNumber() + ": ");
	return semantError();
    }

    /** Increments semantic error count and returns the print stream for
     * error messages.
     *
     * @return a print stream to which the error message is
     * to be printed.
     *
     * */
    public PrintStream semantError() {
	semantErrors++;
	return errorStream;
    }

    /** Returns true if there are any static semantic errors. */
    public boolean errors() {
	return semantErrors != 0;
    }
}
			  
    

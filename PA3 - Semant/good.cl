class Main {
 main(): Object {42 }; 
};

class A {
    foo(a:Int, b: B, c:A, d:B) : A {
       self
    };
  
};

class B inherits A {

    moo() : A {
       let b:B <- new B in
       foo(4, b, b, b)
    };  
};
(*class C {
	a : Int;
	b : Bool;
	init(x : Int, y : Bool) : C {
           {
		a <- x;
		b <- y;
		self;
           }
	};
};

Class Main {
	main():C {
	  (new C).init(1,true)
	};
};*)

# start of generated code
	.data
	.align	2
	.globl	class_nameTab
	.globl	Main_protObj
	.globl	Int_protObj
	.globl	String_protObj
	.globl	bool_const0
	.globl	bool_const1
	.globl	_int_tag
	.globl	_bool_tag
	.globl	_string_tag
_int_tag:
	.word	2
_bool_tag:
	.word	3
_string_tag:
	.word	4
	.globl	_MemMgr_INITIALIZER
_MemMgr_INITIALIZER:
	.word	_NoGC_Init
	.globl	_MemMgr_COLLECTOR
_MemMgr_COLLECTOR:
	.word	_NoGC_Collect
	.globl	_MemMgr_TEST
_MemMgr_TEST:
	.word	0
	.word	-1
str_const11:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const0
	.byte	0	
	.align	2
	.word	-1
str_const10:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Main"
	.byte	0	
	.align	2
	.word	-1
str_const9:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"String"
	.byte	0	
	.align	2
	.word	-1
str_const8:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Bool"
	.byte	0	
	.align	2
	.word	-1
str_const7:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const5
	.ascii	"Int"
	.byte	0	
	.align	2
	.word	-1
str_const6:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const6
	.ascii	"IO"
	.byte	0	
	.align	2
	.word	-1
str_const5:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const4
	.ascii	"Object"
	.byte	0	
	.align	2
	.word	-1
str_const4:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const7
	.ascii	"_prim_slot"
	.byte	0	
	.align	2
	.word	-1
str_const3:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"SELF_TYPE"
	.byte	0	
	.align	2
	.word	-1
str_const2:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"_no_class"
	.byte	0	
	.align	2
	.word	-1
str_const1:
	.word	4
	.word	8
	.word	String_dispTab
	.word	int_const9
	.ascii	"<basic class>"
	.byte	0	
	.align	2
	.word	-1
str_const0:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const10
	.ascii	"./not.cl"
	.byte	0	
	.align	2
	.word	-1
int_const10:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	8
	.word	-1
int_const9:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	13
	.word	-1
int_const8:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	9
	.word	-1
int_const7:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	10
	.word	-1
int_const6:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	2
	.word	-1
int_const5:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	3
	.word	-1
int_const4:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	6
	.word	-1
int_const3:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	4
	.word	-1
int_const2:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	1
	.word	-1
int_const1:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	100
	.word	-1
int_const0:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
bool_const0:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
bool_const1:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	1
class_nameTab:
	.word	str_const5
	.word	str_const6
	.word	str_const7
	.word	str_const8
	.word	str_const9
	.word	str_const10
class_objTab:
	.word	Object_protObj
	.word	Object_init
	.word	IO_protObj
	.word	IO_init
	.word	Int_protObj
	.word	Int_init
	.word	Bool_protObj
	.word	Bool_init
	.word	String_protObj
	.word	String_init
	.word	Main_protObj
	.word	Main_init
Object_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
IO_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	IO.out_string
	.word	IO.out_int
	.word	IO.in_string
	.word	IO.in_int
Int_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
Bool_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
String_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	String.length
	.word	String.concat
	.word	String.substr
Main_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	Main.main
	.word	-1
Object_protObj:
	.word	0
	.word	3
	.word	Object_dispTab
	.word	-1
IO_protObj:
	.word	1
	.word	3
	.word	IO_dispTab
	.word	-1
Int_protObj:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
Bool_protObj:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
String_protObj:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const0
	.word	0
	.word	-1
Main_protObj:
	.word	5
	.word	3
	.word	Main_dispTab
	.globl	heap_start
heap_start:
	.word	0
	.text
	.globl	Main_init
	.globl	Int_init
	.globl	String_init
	.globl	Bool_init
	.globl	Main.main
#Object
Object_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#IO
IO_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#Int
Int_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#Bool
Bool_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#String
String_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#Main
Main_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
Main.main:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	#let
		#identifier:x
		#type_decl:Int
		#init
	#int_const
	la	$a0 int_const0
	#int_const_end
	sw	$a0 -4($fp)
		#init_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
		#body
	#let
		#identifier:b
		#type_decl:Bool
	la	$a0 bool_const0
	sw	$a0 0($sp)
	addiu	$sp $sp -4
		#body
	#block
	#loop
label0:
	#lt
	#object: x
	lw	$a0 -4($fp)
	#object_end
	lw	$a0 12($a0)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#int_const
	la	$a0 int_const1
	#int_const_end
	lw	$t1 4($sp)
	lw	$t2 12($a0)
	addiu	$sp $sp 4
	la	$a0 bool_const1
	blt	$t1 $t2 label2
	la	$a0 bool_const0
label2:
	#lt_end
	lw	$t1 12($a0)
	beq	$t1 $zero label1
	#block
	#eq
	#object: b
	lw	$a0 -8($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#object: b
	lw	$a0 -8($fp)
	#object_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label3
	la	$a0 bool_const0
label3:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label4
	la	$a0 bool_const0
label4:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label5
	la	$a0 bool_const0
label5:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label6
	la	$a0 bool_const0
label6:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label7
	la	$a0 bool_const0
label7:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label8
	la	$a0 bool_const0
label8:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label9
	la	$a0 bool_const0
label9:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label10
	la	$a0 bool_const0
label10:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label11
	la	$a0 bool_const0
label11:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label12
	la	$a0 bool_const0
label12:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label13
	la	$a0 bool_const0
label13:
	#comp(not)_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label14
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label14:
	#eq
	#object: b
	lw	$a0 -8($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#object: b
	lw	$a0 -8($fp)
	#object_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label15
	la	$a0 bool_const0
label15:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label16
	la	$a0 bool_const0
label16:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label17
	la	$a0 bool_const0
label17:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label18
	la	$a0 bool_const0
label18:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label19
	la	$a0 bool_const0
label19:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label20
	la	$a0 bool_const0
label20:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label21
	la	$a0 bool_const0
label21:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label22
	la	$a0 bool_const0
label22:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label23
	la	$a0 bool_const0
label23:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label24
	la	$a0 bool_const0
label24:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label25
	la	$a0 bool_const0
label25:
	#comp(not)_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label26
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label26:
	#eq
	#object: b
	lw	$a0 -8($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#object: b
	lw	$a0 -8($fp)
	#object_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label27
	la	$a0 bool_const0
label27:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label28
	la	$a0 bool_const0
label28:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label29
	la	$a0 bool_const0
label29:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label30
	la	$a0 bool_const0
label30:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label31
	la	$a0 bool_const0
label31:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label32
	la	$a0 bool_const0
label32:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label33
	la	$a0 bool_const0
label33:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label34
	la	$a0 bool_const0
label34:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label35
	la	$a0 bool_const0
label35:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label36
	la	$a0 bool_const0
label36:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label37
	la	$a0 bool_const0
label37:
	#comp(not)_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label38
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label38:
	#eq
	#object: b
	lw	$a0 -8($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#object: b
	lw	$a0 -8($fp)
	#object_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label39
	la	$a0 bool_const0
label39:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label40
	la	$a0 bool_const0
label40:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label41
	la	$a0 bool_const0
label41:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label42
	la	$a0 bool_const0
label42:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label43
	la	$a0 bool_const0
label43:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label44
	la	$a0 bool_const0
label44:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label45
	la	$a0 bool_const0
label45:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label46
	la	$a0 bool_const0
label46:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label47
	la	$a0 bool_const0
label47:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label48
	la	$a0 bool_const0
label48:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label49
	la	$a0 bool_const0
label49:
	#comp(not)_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label50
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label50:
	#eq
	#object: b
	lw	$a0 -8($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#comp(not)
	#object: b
	lw	$a0 -8($fp)
	#object_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label51
	la	$a0 bool_const0
label51:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label52
	la	$a0 bool_const0
label52:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label53
	la	$a0 bool_const0
label53:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label54
	la	$a0 bool_const0
label54:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label55
	la	$a0 bool_const0
label55:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label56
	la	$a0 bool_const0
label56:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label57
	la	$a0 bool_const0
label57:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label58
	la	$a0 bool_const0
label58:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label59
	la	$a0 bool_const0
label59:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label60
	la	$a0 bool_const0
label60:
	#comp(not)_end
	lw	$t1 12($a0)
	la	$a0 bool_const1
	beqz	$t1 label61
	la	$a0 bool_const0
label61:
	#comp(not)_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label62
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label62:
	#assign
	#plus
	#object: x
	lw	$a0 -4($fp)
	#object_end
	lw	$a0 12($a0)
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#int_const
	la	$a0 int_const2
	#int_const_end
	jal Object.copy
	lw	$t1 4($sp)
	lw	$t2 12($a0)
	add	$t1 $t1 $t2
	sw	$t1 12($a0)
	addiu	$sp $sp 4
	#plus_end
	sw	$a0 -4($fp)
	#assign_end
	#block_end
	b	label0
label1:
	move	$a0 $zero
	#loop_end
	#if
	#pred
	#object: b
	lw	$a0 -8($fp)
	#object_end
	lw	$t1 12($a0)
	beqz	$t1 label63
	#then

	#dispatch:abort
		#pushing args
		#args pushed
	#e0
	#object: self
	move	$a0 $s0
	#object_end
	#e0
	bne	$a0 $zero label65
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label65:
	lw	$t1 8($a0)
	#name_:abort
	#[abort, type_name, copy, Mainmain]
	#index:0
	lw	$t1 0($t1)
	jalr	$t1
	#dispatch_end

	b	label64
	#else
label63:
	#int_const
	la	$a0 int_const0
	#int_const_end
label64:
	#if_end
	#block_end
	addiu	$sp $sp 4
	#let_end
	addiu	$sp $sp 4
	#let_end
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra

# end of generated code

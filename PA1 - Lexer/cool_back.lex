/*
 *  The scanner definition for COOL.
 */

import java_cup.runtime.Symbol;
import java.util.*;

%%

%{

/*  Stuff enclosed in %{ %} is copied verbatim to the lexer class
 *  definition, all the extra variables/functions you want to use in the
 *  lexer actions should go here.  Don't remove or modify anything that
 *  was there initially.  */

    // Max size of string constants
    static int MAX_STR_CONST = 1025;

    // For assembling string constants
    StringBuffer string_buf = new StringBuffer();

    private int curr_lineno = 0;
    int get_curr_lineno() {
	return curr_lineno;
    }

    private AbstractSymbol filename;

    void set_filename(String fname) {
	filename = AbstractTable.stringtable.addString(fname);
    }

    AbstractSymbol curr_filename() {
	return filename;
    }
    int cont = 0, pos = 0;
    String str = "", ret = "", s = "", aux = "", linecount = "";
    boolean nl = false;
%}

%init{

/*  Stuff enclosed in %init{ %init} is copied verbatim to the lexer
 *  class constructor, all the extra initialization you want to do should
 *  go here.  Don't remove or modify anything that was there initially. */

    // empty for now
%init}

%eofval{

/*  Stuff enclosed in %eofval{ %eofval} specifies java code that is
 *  executed when end-of-file is reached.  If you use multiple lexical
 *  states and want to do something special if an EOF is encountered in
 *  one of those states, place your code in the switch statement.
 *  Ultimately, you should return the EOF symbol, or your lexer won't
 *  work.  */

    switch(yy_lexical_state) {
    case YYINITIAL:
        break;
    case STRING:
        yybegin(END);
        return new Symbol(TokenConstants.ERROR, "EOF in string constant");
    case COMMENT:
        yybegin(END);
        return new Symbol(TokenConstants.ERROR, "EOF in comment");
    case END:
        return new Symbol(TokenConstants.EOF);
	/* nothing special to do in the initial state */
	/* If necessary, add code for other states here, e.g:
	   case COMMENT:
	   ...
	   break;
	*/
    }
    return new Symbol(TokenConstants.EOF);
%eofval}

%class CoolLexer
%cup
%state COMMENT, STRING, BACKSLASH, END, ERROR_STRING, ERROR_STRING1
<DIGIT>=<[0-9]>
%%

<YYINITIAL>[Ii][Nn][Hh][Ee][Rr][Ii][Tt][Ss]     {curr_lineno++; return new Symbol(TokenConstants.INHERITS);}
<YYINITIAL>[Cc][Aa][Ss][Ee]                     {curr_lineno++; return new Symbol(TokenConstants.CASE);}
<YYINITIAL>[Cc][Ll][Aa][Ss][Ss]                 {curr_lineno++; return new Symbol(TokenConstants.CLASS);}
<YYINITIAL>[Ww][Hh][Ii][Ll][Ee]                 {curr_lineno++; return new Symbol(TokenConstants.WHILE);}
<YYINITIAL>[Ll][Oo][Oo][Pp]                     {curr_lineno++; return new Symbol(TokenConstants.LOOP);}
<YYINITIAL>[Pp][Oo][Oo][Ll]                     {curr_lineno++; return new Symbol(TokenConstants.POOL);}
<YYINITIAL>[Ll][Ee][Tt]                         {curr_lineno++; return new Symbol(TokenConstants.LET);}
<YYINITIAL>[Ii][Ff]                             {curr_lineno++; return new Symbol(TokenConstants.IF);}
<YYINITIAL>[Tt][Hh][Ee][Nn]                     {curr_lineno++; return new Symbol(TokenConstants.THEN);}
<YYINITIAL>[Ee][Ll][Ss][Ee]                     {curr_lineno++; return new Symbol(TokenConstants.ELSE);}
<YYINITIAL>[Ff][Ii]                             {curr_lineno++; return new Symbol(TokenConstants.FI);}
<YYINITIAL>[Ii][Ss][Vv][Oo][Ii][Dd]             {curr_lineno++; return new Symbol(TokenConstants.ISVOID);}
<YYINITIAL>[Ee][Ss][Aa][Cc]                     {curr_lineno++; return new Symbol(TokenConstants.ESAC);}
<YYINITIAL>[Ii][Nn]                             {curr_lineno++; return new Symbol(TokenConstants.IN);}
<YYINITIAL>[Nn][Oo][Tt]                         {curr_lineno++; return new Symbol(TokenConstants.NOT);}
<YYINITIAL>[Oo][Ff]                             {curr_lineno++; return new Symbol(TokenConstants.OF);}
<YYINITIAL>[Nn][Ee][Ww]                         {curr_lineno++; return new Symbol(TokenConstants.NEW);}
<YYINITIAL>[t][Rr][Uu][Ee]                     {curr_lineno++; return new Symbol(TokenConstants.BOOL_CONST, yytext()); }
<YYINITIAL>[f][Aa][Ll][Ss][Ee]                 {curr_lineno++; return new Symbol(TokenConstants.BOOL_CONST, yytext()); }

<YYINITIAL>"=>"         {curr_lineno++; return new Symbol(TokenConstants.DARROW);}
<YYINITIAL>"<="         {curr_lineno++; return new Symbol(TokenConstants.LE);}
<YYINITIAL>"<-"         {curr_lineno++; return new Symbol(TokenConstants.ASSIGN);}
<YYINITIAL>"<"          {curr_lineno++; return new Symbol(TokenConstants.LT);}
<YYINITIAL>"="          {curr_lineno++; return new Symbol(TokenConstants.EQ);}
<YYINITIAL>"*"          {curr_lineno++; return new Symbol(TokenConstants.MULT);}
<YYINITIAL>";"          {curr_lineno++; return new Symbol(TokenConstants.SEMI);} 
<YYINITIAL>"~"          {curr_lineno++; return new Symbol(TokenConstants.NEG);}
<YYINITIAL>"("          {curr_lineno++; return new Symbol(TokenConstants.LPAREN);}
<YYINITIAL>"-"          {curr_lineno++; return new Symbol(TokenConstants.MINUS);}
<YYINITIAL>")"          {curr_lineno++; return new Symbol(TokenConstants.RPAREN);}
<YYINITIAL>","          {curr_lineno++; return new Symbol(TokenConstants.COMMA);}
<YYINITIAL>"/"          {curr_lineno++; return new Symbol(TokenConstants.DIV);}
<YYINITIAL>"+"          {curr_lineno++; return new Symbol(TokenConstants.PLUS);}
<YYINITIAL>":"          {curr_lineno++; return new Symbol(TokenConstants.COLON);}
<YYINITIAL>"{"          {curr_lineno++; return new Symbol(TokenConstants.LBRACE);}
<YYINITIAL>"}"          {curr_lineno++; return new Symbol(TokenConstants.RBRACE);}
<YYINITIAL>"."          {curr_lineno++; return new Symbol(TokenConstants.DOT);}
<YYINITIAL>"@"          {curr_lineno++; return new Symbol(TokenConstants.AT);}

<YYINITIAL>\_         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "_");}
<YYINITIAL>\!         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "!");}
<YYINITIAL>\#         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "#");}
<YYINITIAL>\$         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "$");}
<YYINITIAL>\%         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "%");}
<YYINITIAL>"^"        {curr_lineno++; return new Symbol(TokenConstants.ERROR, "^");}
<YYINITIAL>\&         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "&");}
<YYINITIAL>\>         {curr_lineno++; return new Symbol(TokenConstants.ERROR, ">");}
<YYINITIAL>\?         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "?");}
<YYINITIAL>\`         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "`");}
<YYINITIAL>\[         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "[");}
<YYINITIAL>\]         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "]");}
<YYINITIAL>\\         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "\\");}
<YYINITIAL>\|         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "|");}
<YYINITIAL>""         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "\001");}
<YYINITIAL>""         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "\002");}
<YYINITIAL>""         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "\003");}
<YYINITIAL>""         {curr_lineno++; return new Symbol(TokenConstants.ERROR, "\004");}
<YYINITIAL>\*\)       {return new Symbol(TokenConstants.ERROR, "Unmatched *)");}

<YYINITIAL>[\ \t\b\012\n\x0b\f\r]?    {yybegin(YYINITIAL);}
<YYINITIAL>((\-\-).*)?              {curr_lineno++; yybegin(YYINITIAL);}

<YYINITIAL>(\(\*)?                      {curr_lineno++; cont++; yybegin(COMMENT);}
<COMMENT>[^\*]*(\*)*(\*\))*      {
                                    linecount = yytext();
                                    while (linecount.contains("\n")){
                                        curr_lineno++;
                                        linecount = linecount.replaceFirst("\n",".");                   
                                    } 
                                    
                                    if (yytext().contains("(**)")) {
                                        yybegin(COMMENT);
                                    } else if (yytext().length()<2){
                                        yybegin(COMMENT); 
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("*)")) {
                                        aux = yytext();
                                        while (true){
                                            if (aux.length()>1){
                                                if (aux.substring(aux.length()-2,aux.length()).equals("*)")){
                                                    aux = aux.substring(0,aux.length()-2);
                                                    cont--;
                                                } else {
                                                    break;
                                                } 
                                            } else {
                                                break;
                                            }                                            
                                        }
                                        if (cont == 0){
                                            yybegin(YYINITIAL);
                                        } else {
                                            yybegin(COMMENT);
                                        }                                        
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("(*")){
                                        cont++;
                                    } else {
                                        yybegin(COMMENT); 
                                    }                                                                    
                                }

<YYINITIAL>[A-Z][a-zA-Z0-9_]*               {curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}

<YYINITIAL>[a-z][a-zA-Z0-9_]*               {curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}

<YYINITIAL>[0-9]+                           {curr_lineno++; return new Symbol(TokenConstants.INT_CONST, AbstractTable.inttable.addString(yytext()));}

<YYINITIAL>\"                           {curr_lineno++; yybegin(STRING);}
<STRING>[^\"\n\0\\"\""]*                {str=str.concat(yytext());
                                                if (str.contains("\\n")){
                                                    str = str.replace("\\n","\n");                                                    
                                                }
                                                if (str.contains("\\t")){
                                                    str = str.replace("\\t","\t");
                                                }
                                                if (str.contains("\\f")){
                                                    str = str.replace("\\f","\f");
                                                }
                                                if (str.contains("\\b")){
                                                    str = str.replace("\\b","\b");
                                                }                                    
                                        }
<STRING>[^\"\n\0\\]*\\[tfbn]?[\n]?      {                                    
                                            pos = yytext().length();
                                            if (pos>=2){
                                                s = yytext().substring(pos-1,pos);
                                            }                                        
                                            if (pos>1 && (!(s.equals("t") || s.equals("f") || s.equals("b") || s.equals("n") || s.equals("\"")))) {
                                                if (yytext().substring(pos-2,pos).equals("\\\n")){
                                                    str=str.concat(yytext().substring(0,pos-2).concat("\n"));
                                                    nl = true;
                                                } else {
                                                    str=str.concat(yytext().substring(0,pos-1));
                                                }
                                            } else if (pos>1){                                               
                                                str=str.concat(yytext());
                                                if (str.contains("\\n")){
                                                    str = str.replace("\\n","\n");                                                    
                                                }
                                                if (str.contains("\\t")){
                                                    str = str.replace("\\t","\t");
                                                }
                                                if (str.contains("\\f")){
                                                    str = str.replace("\\f","\f");
                                                }
                                                if (str.contains("\\b")){
                                                    str = str.replace("\\b","\b");
                                                }
                                            }
                                            if (pos>1){
                                                if (yytext().equals("\\\n")){
                                                } else if (str.substring(str.length()-1,str.length()).equals("\\")){
                                                    nl = true;
                                                }
                                            }                                                                                 
                                        }
<STRING>[^\"\n\0\\]*\\\"                {   
                                            pos = yytext().length();
                                            if (pos>1 && yytext().substring(pos-2,pos).equals("\\\"")) {
                                                nl = true;
                                                if (yytext().equals("\\\"")){
                                                    str=str.concat(yytext().substring(0,pos-2).concat("\""));
                                                } else if (yytext().contains("\\\"")){
                                                    str = str.concat(yytext());
                                                    str = str.replace("\\\"","\"");
                                                } else {
                                                    str = str.concat(yytext());
                                                }                                             
                                            }
                                        }                                                                                
<STRING>[^\"\n\0\\]*\\\\                { 
                                            if (yytext().length()>1) {
                                                str=str.concat(yytext().substring(0,yytext().length()-1));
                                            }
                                        }    
<STRING>[\n]                            {  
                                            if (nl==true){
                                                str=str.concat("\n");
                                                nl = false;
                                            } else if (str.length()>0){
                                                if (nl == false && !str.substring(str.length()-1,str.length()).equals("\\")){
                                                    yybegin(YYINITIAL);
                                                    nl = false;                                                
                                                    str = "";  
                                                    return new Symbol(TokenConstants.ERROR, "Unterminated string constant");  
                                                }                                                                                               
                                            } 
                                            if (nl == false){
                                                yybegin(YYINITIAL);
                                                nl = false;                                                
                                                str = "";
                                                return new Symbol(TokenConstants.ERROR, "Unterminated string constant");                                                
                                            }                                            
                                        }                                                                                 
<STRING>\"                              {                                                
                                            ret=str; 
                                            str=""; 
                                            yybegin(YYINITIAL); 
                                            nl = false;
                                            if (ret.length()>=MAX_STR_CONST){
                                                yybegin(ERROR_STRING1);
                                            } else {
                                                return new Symbol(TokenConstants.STR_CONST, AbstractTable.stringtable.addString(ret));
                                            }                                            
                                        }
<ERROR_STRING1>.*                       {yybegin(YYINITIAL); return new Symbol(TokenConstants.ERROR, "String constant too long");}

.   {System.err.println("LEXER BUG - UNMATCHED: " + yytext()); }

/*
 *  The scanner definition for COOL.
 */
import java_cup.runtime.Symbol;
import java.util.*;


class CoolLexer implements java_cup.runtime.Scanner {
	private final int YY_BUFFER_SIZE = 512;
	private final int YY_F = -1;
	private final int YY_NO_STATE = -1;
	private final int YY_NOT_ACCEPT = 0;
	private final int YY_START = 1;
	private final int YY_END = 2;
	private final int YY_NO_ANCHOR = 4;
	private final int YY_BOL = 128;
	private final int YY_EOF = 129;

/*  Stuff enclosed in %{ %} is copied verbatim to the lexer class
 *  definition, all the extra variables/functions you want to use in the
 *  lexer actions should go here.  Don't remove or modify anything that
 *  was there initially.  */
    // Max size of string constants
    static int MAX_STR_CONST = 1025;
    // For assembling string constants
    StringBuffer string_buf = new StringBuffer();
    private int curr_lineno = 0;
    int get_curr_lineno() {
	return curr_lineno;
    }
    private AbstractSymbol filename;
    void set_filename(String fname) {
	filename = AbstractTable.stringtable.addString(fname);
    }
    AbstractSymbol curr_filename() {
	return filename;
    }
    int cont = 0, pos = 0;
    String str = "", ret = "", s = "", aux = "", linecount = "";
    boolean nl = false;
	private java.io.BufferedReader yy_reader;
	private int yy_buffer_index;
	private int yy_buffer_read;
	private int yy_buffer_start;
	private int yy_buffer_end;
	private char yy_buffer[];
	private boolean yy_at_bol;
	private int yy_lexical_state;

	CoolLexer (java.io.Reader reader) {
		this ();
		if (null == reader) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(reader);
	}

	CoolLexer (java.io.InputStream instream) {
		this ();
		if (null == instream) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(new java.io.InputStreamReader(instream));
	}

	private CoolLexer () {
		yy_buffer = new char[YY_BUFFER_SIZE];
		yy_buffer_read = 0;
		yy_buffer_index = 0;
		yy_buffer_start = 0;
		yy_buffer_end = 0;
		yy_at_bol = true;
		yy_lexical_state = YYINITIAL;

/*  Stuff enclosed in %init{ %init} is copied verbatim to the lexer
 *  class constructor, all the extra initialization you want to do should
 *  go here.  Don't remove or modify anything that was there initially. */
    // empty for now
	}

	private boolean yy_eof_done = false;
	private final int STRING = 2;
	private final int YYINITIAL = 0;
	private final int BACKSLASH = 3;
	private final int COMMENT = 1;
	private final int ERROR_STRING = 5;
	private final int END = 4;
	private final int ERROR_STRING1 = 6;
	private final int yy_state_dtrans[] = {
		0,
		66,
		68,
		96,
		96,
		96,
		74
	};
	private void yybegin (int state) {
		yy_lexical_state = state;
	}
	private int yy_advance ()
		throws java.io.IOException {
		int next_read;
		int i;
		int j;

		if (yy_buffer_index < yy_buffer_read) {
			return yy_buffer[yy_buffer_index++];
		}

		if (0 != yy_buffer_start) {
			i = yy_buffer_start;
			j = 0;
			while (i < yy_buffer_read) {
				yy_buffer[j] = yy_buffer[i];
				++i;
				++j;
			}
			yy_buffer_end = yy_buffer_end - yy_buffer_start;
			yy_buffer_start = 0;
			yy_buffer_read = j;
			yy_buffer_index = j;
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}

		while (yy_buffer_index >= yy_buffer_read) {
			if (yy_buffer_index >= yy_buffer.length) {
				yy_buffer = yy_double(yy_buffer);
			}
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}
		return yy_buffer[yy_buffer_index++];
	}
	private void yy_move_end () {
		if (yy_buffer_end > yy_buffer_start &&
		    '\n' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
		if (yy_buffer_end > yy_buffer_start &&
		    '\r' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
	}
	private boolean yy_last_was_cr=false;
	private void yy_mark_start () {
		yy_buffer_start = yy_buffer_index;
	}
	private void yy_mark_end () {
		yy_buffer_end = yy_buffer_index;
	}
	private void yy_to_mark () {
		yy_buffer_index = yy_buffer_end;
		yy_at_bol = (yy_buffer_end > yy_buffer_start) &&
		            ('\r' == yy_buffer[yy_buffer_end-1] ||
		             '\n' == yy_buffer[yy_buffer_end-1] ||
		             2028/*LS*/ == yy_buffer[yy_buffer_end-1] ||
		             2029/*PS*/ == yy_buffer[yy_buffer_end-1]);
	}
	private java.lang.String yytext () {
		return (new java.lang.String(yy_buffer,
			yy_buffer_start,
			yy_buffer_end - yy_buffer_start));
	}
	private int yylength () {
		return yy_buffer_end - yy_buffer_start;
	}
	private char[] yy_double (char buf[]) {
		int i;
		char newbuf[];
		newbuf = new char[2*buf.length];
		for (i = 0; i < buf.length; ++i) {
			newbuf[i] = buf[i];
		}
		return newbuf;
	}
	private final int YY_E_INTERNAL = 0;
	private final int YY_E_MATCH = 1;
	private java.lang.String yy_error_string[] = {
		"Error: Internal error.\n",
		"Error: Unmatched input.\n"
	};
	private void yy_error (int code,boolean fatal) {
		java.lang.System.out.print(yy_error_string[code]);
		java.lang.System.out.flush();
		if (fatal) {
			throw new Error("Fatal Error.\n");
		}
	}
	private int[][] unpackFromString(int size1, int size2, String st) {
		int colonIndex = -1;
		String lengthString;
		int sequenceLength = 0;
		int sequenceInteger = 0;

		int commaIndex;
		String workString;

		int res[][] = new int[size1][size2];
		for (int i= 0; i < size1; i++) {
			for (int j= 0; j < size2; j++) {
				if (sequenceLength != 0) {
					res[i][j] = sequenceInteger;
					sequenceLength--;
					continue;
				}
				commaIndex = st.indexOf(',');
				workString = (commaIndex==-1) ? st :
					st.substring(0, commaIndex);
				st = st.substring(commaIndex+1);
				colonIndex = workString.indexOf(':');
				if (colonIndex == -1) {
					res[i][j]=Integer.parseInt(workString);
					continue;
				}
				lengthString =
					workString.substring(colonIndex+1);
				sequenceLength=Integer.parseInt(lengthString);
				workString=workString.substring(0,colonIndex);
				sequenceInteger=Integer.parseInt(workString);
				res[i][j] = sequenceInteger;
				sequenceLength--;
			}
		}
		return res;
	}
	private int yy_acpt[] = {
		/* 0 */ YY_NO_ANCHOR,
		/* 1 */ YY_NO_ANCHOR,
		/* 2 */ YY_NO_ANCHOR,
		/* 3 */ YY_NO_ANCHOR,
		/* 4 */ YY_NO_ANCHOR,
		/* 5 */ YY_NO_ANCHOR,
		/* 6 */ YY_NO_ANCHOR,
		/* 7 */ YY_NO_ANCHOR,
		/* 8 */ YY_NO_ANCHOR,
		/* 9 */ YY_NO_ANCHOR,
		/* 10 */ YY_NO_ANCHOR,
		/* 11 */ YY_NO_ANCHOR,
		/* 12 */ YY_NO_ANCHOR,
		/* 13 */ YY_NO_ANCHOR,
		/* 14 */ YY_NO_ANCHOR,
		/* 15 */ YY_NO_ANCHOR,
		/* 16 */ YY_NO_ANCHOR,
		/* 17 */ YY_NO_ANCHOR,
		/* 18 */ YY_NO_ANCHOR,
		/* 19 */ YY_NO_ANCHOR,
		/* 20 */ YY_NO_ANCHOR,
		/* 21 */ YY_NO_ANCHOR,
		/* 22 */ YY_NO_ANCHOR,
		/* 23 */ YY_NO_ANCHOR,
		/* 24 */ YY_NO_ANCHOR,
		/* 25 */ YY_NO_ANCHOR,
		/* 26 */ YY_NO_ANCHOR,
		/* 27 */ YY_NO_ANCHOR,
		/* 28 */ YY_NO_ANCHOR,
		/* 29 */ YY_NO_ANCHOR,
		/* 30 */ YY_NO_ANCHOR,
		/* 31 */ YY_NO_ANCHOR,
		/* 32 */ YY_NO_ANCHOR,
		/* 33 */ YY_NO_ANCHOR,
		/* 34 */ YY_NO_ANCHOR,
		/* 35 */ YY_NO_ANCHOR,
		/* 36 */ YY_NO_ANCHOR,
		/* 37 */ YY_NO_ANCHOR,
		/* 38 */ YY_NO_ANCHOR,
		/* 39 */ YY_NO_ANCHOR,
		/* 40 */ YY_NO_ANCHOR,
		/* 41 */ YY_NO_ANCHOR,
		/* 42 */ YY_NO_ANCHOR,
		/* 43 */ YY_NO_ANCHOR,
		/* 44 */ YY_NO_ANCHOR,
		/* 45 */ YY_NO_ANCHOR,
		/* 46 */ YY_NO_ANCHOR,
		/* 47 */ YY_NO_ANCHOR,
		/* 48 */ YY_NO_ANCHOR,
		/* 49 */ YY_NO_ANCHOR,
		/* 50 */ YY_NO_ANCHOR,
		/* 51 */ YY_NO_ANCHOR,
		/* 52 */ YY_NO_ANCHOR,
		/* 53 */ YY_NO_ANCHOR,
		/* 54 */ YY_NO_ANCHOR,
		/* 55 */ YY_NO_ANCHOR,
		/* 56 */ YY_NO_ANCHOR,
		/* 57 */ YY_NO_ANCHOR,
		/* 58 */ YY_NO_ANCHOR,
		/* 59 */ YY_NO_ANCHOR,
		/* 60 */ YY_NO_ANCHOR,
		/* 61 */ YY_NO_ANCHOR,
		/* 62 */ YY_NO_ANCHOR,
		/* 63 */ YY_NO_ANCHOR,
		/* 64 */ YY_NO_ANCHOR,
		/* 65 */ YY_NO_ANCHOR,
		/* 66 */ YY_NO_ANCHOR,
		/* 67 */ YY_NOT_ACCEPT,
		/* 68 */ YY_NO_ANCHOR,
		/* 69 */ YY_NO_ANCHOR,
		/* 70 */ YY_NO_ANCHOR,
		/* 71 */ YY_NO_ANCHOR,
		/* 72 */ YY_NO_ANCHOR,
		/* 73 */ YY_NO_ANCHOR,
		/* 74 */ YY_NO_ANCHOR,
		/* 75 */ YY_NO_ANCHOR,
		/* 76 */ YY_NO_ANCHOR,
		/* 77 */ YY_NO_ANCHOR,
		/* 78 */ YY_NO_ANCHOR,
		/* 79 */ YY_NO_ANCHOR,
		/* 80 */ YY_NO_ANCHOR,
		/* 81 */ YY_NO_ANCHOR,
		/* 82 */ YY_NO_ANCHOR,
		/* 83 */ YY_NO_ANCHOR,
		/* 84 */ YY_NO_ANCHOR,
		/* 85 */ YY_NO_ANCHOR,
		/* 86 */ YY_NO_ANCHOR,
		/* 87 */ YY_NO_ANCHOR,
		/* 88 */ YY_NO_ANCHOR,
		/* 89 */ YY_NO_ANCHOR,
		/* 90 */ YY_NO_ANCHOR,
		/* 91 */ YY_NO_ANCHOR,
		/* 92 */ YY_NO_ANCHOR,
		/* 93 */ YY_NO_ANCHOR,
		/* 94 */ YY_NO_ANCHOR,
		/* 95 */ YY_NO_ANCHOR,
		/* 96 */ YY_NOT_ACCEPT,
		/* 97 */ YY_NO_ANCHOR,
		/* 98 */ YY_NO_ANCHOR,
		/* 99 */ YY_NO_ANCHOR,
		/* 100 */ YY_NO_ANCHOR,
		/* 101 */ YY_NO_ANCHOR,
		/* 102 */ YY_NO_ANCHOR,
		/* 103 */ YY_NO_ANCHOR,
		/* 104 */ YY_NO_ANCHOR,
		/* 105 */ YY_NO_ANCHOR,
		/* 106 */ YY_NO_ANCHOR,
		/* 107 */ YY_NO_ANCHOR,
		/* 108 */ YY_NO_ANCHOR,
		/* 109 */ YY_NO_ANCHOR,
		/* 110 */ YY_NO_ANCHOR,
		/* 111 */ YY_NO_ANCHOR,
		/* 112 */ YY_NO_ANCHOR,
		/* 113 */ YY_NO_ANCHOR,
		/* 114 */ YY_NO_ANCHOR,
		/* 115 */ YY_NO_ANCHOR,
		/* 116 */ YY_NO_ANCHOR,
		/* 117 */ YY_NO_ANCHOR,
		/* 118 */ YY_NO_ANCHOR,
		/* 119 */ YY_NO_ANCHOR,
		/* 120 */ YY_NO_ANCHOR,
		/* 121 */ YY_NO_ANCHOR,
		/* 122 */ YY_NO_ANCHOR,
		/* 123 */ YY_NO_ANCHOR,
		/* 124 */ YY_NO_ANCHOR,
		/* 125 */ YY_NO_ANCHOR,
		/* 126 */ YY_NO_ANCHOR,
		/* 127 */ YY_NO_ANCHOR,
		/* 128 */ YY_NO_ANCHOR,
		/* 129 */ YY_NO_ANCHOR,
		/* 130 */ YY_NO_ANCHOR,
		/* 131 */ YY_NO_ANCHOR,
		/* 132 */ YY_NO_ANCHOR,
		/* 133 */ YY_NO_ANCHOR,
		/* 134 */ YY_NO_ANCHOR,
		/* 135 */ YY_NO_ANCHOR,
		/* 136 */ YY_NO_ANCHOR,
		/* 137 */ YY_NO_ANCHOR,
		/* 138 */ YY_NO_ANCHOR,
		/* 139 */ YY_NO_ANCHOR,
		/* 140 */ YY_NO_ANCHOR,
		/* 141 */ YY_NO_ANCHOR,
		/* 142 */ YY_NO_ANCHOR,
		/* 143 */ YY_NO_ANCHOR,
		/* 144 */ YY_NO_ANCHOR,
		/* 145 */ YY_NO_ANCHOR,
		/* 146 */ YY_NO_ANCHOR,
		/* 147 */ YY_NO_ANCHOR,
		/* 148 */ YY_NO_ANCHOR,
		/* 149 */ YY_NO_ANCHOR,
		/* 150 */ YY_NO_ANCHOR,
		/* 151 */ YY_NO_ANCHOR,
		/* 152 */ YY_NO_ANCHOR,
		/* 153 */ YY_NO_ANCHOR,
		/* 154 */ YY_NO_ANCHOR,
		/* 155 */ YY_NO_ANCHOR,
		/* 156 */ YY_NO_ANCHOR,
		/* 157 */ YY_NO_ANCHOR,
		/* 158 */ YY_NO_ANCHOR,
		/* 159 */ YY_NO_ANCHOR,
		/* 160 */ YY_NO_ANCHOR,
		/* 161 */ YY_NO_ANCHOR,
		/* 162 */ YY_NO_ANCHOR,
		/* 163 */ YY_NO_ANCHOR,
		/* 164 */ YY_NO_ANCHOR,
		/* 165 */ YY_NO_ANCHOR,
		/* 166 */ YY_NO_ANCHOR,
		/* 167 */ YY_NO_ANCHOR,
		/* 168 */ YY_NO_ANCHOR,
		/* 169 */ YY_NO_ANCHOR,
		/* 170 */ YY_NO_ANCHOR,
		/* 171 */ YY_NO_ANCHOR,
		/* 172 */ YY_NO_ANCHOR,
		/* 173 */ YY_NO_ANCHOR,
		/* 174 */ YY_NO_ANCHOR,
		/* 175 */ YY_NO_ANCHOR,
		/* 176 */ YY_NO_ANCHOR,
		/* 177 */ YY_NO_ANCHOR,
		/* 178 */ YY_NO_ANCHOR,
		/* 179 */ YY_NO_ANCHOR,
		/* 180 */ YY_NO_ANCHOR,
		/* 181 */ YY_NO_ANCHOR,
		/* 182 */ YY_NO_ANCHOR,
		/* 183 */ YY_NO_ANCHOR,
		/* 184 */ YY_NO_ANCHOR,
		/* 185 */ YY_NO_ANCHOR
	};
	private int yy_cmap[] = unpackFromString(1,130,
"55,50,51,52,53,76:3,56:2,54,56:2,77,76:18,56,38,75,39,40,41,43,76,27,28,24," +
"31,29,23,35,30,73:10,32,25,22,20,21,44,36,57,58,59,60,61,14,58,62,63,58:2,6" +
"4,58,65,66,67,58,68,69,6,70,71,72,58:3,46,48,47,42,37,45,9,78,8,16,4,19,74," +
"3,1,74:2,10,74,2,12,13,74,5,7,17,18,15,11,74:3,33,49,34,26,76,0:2")[0];

	private int yy_rmap[] = unpackFromString(1,186,
"0,1,2,3,4,1,5,6,7,1:2,8,1:27,9,1,10,11:2,12,1:3,13,1:2,11:5,12,11:9,14,15,1" +
"6,17,1:4,18,1,19,20,21,12:2,11,12:5,11,12:7,22,23,24,25,26,27,28,29,1,30,31" +
",32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56" +
",57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,11,73,74,75,76,77,78,79,80" +
",81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,1" +
"04,105,12,106,107,108,109")[0];

	private int yy_nxt[][] = unpackFromString(110,79,
"1,2,76,147,156,147,3,147,159,147,161,163,100,165,77,147:2,167,147,104,4,5,6" +
",7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32" +
",33,34,35,36,37,75,38,75,181:2,182,181,183,181,101,146,155,105,184,181:4,18" +
"5,39,147,40,38,75,147,-1:80,147,41,147:4,169,147:6,42,147:4,42,-1:17,147,-1" +
":19,147:8,41,147:3,169,147:5,-1:3,147,-1,181:2,158,181:16,-1:17,181,-1:19,1" +
"81:5,158,181:12,-1:3,181,-1:21,45,-1:77,46,-1:2,47,-1:78,48,-1:83,49,-1:74," +
"50,-1:127,39,-1:6,147:2,151,147:16,-1:17,147,-1:19,147:5,151,147:12,-1:3,14" +
"7,-1,147:19,-1:17,147,-1:19,147:18,-1:3,147,-1,181:19,-1:17,181,-1:19,181:1" +
"8,-1:3,181,-1,48:53,-1,48:22,-1,48,1,95:23,102,95:54,-1:28,106,-1:50,1,97:4" +
"7,69,97:5,70,38,97:19,71,97:3,-1:2,98,-1:14,98,-1,98,-1:28,72,-1:5,103,-1:2" +
"0,73,-1:2,98,1,99:53,-1,99:22,-1,99,-1,147:3,107,147:7,109,147:7,-1:17,147," +
"-1:19,147:4,107,147:4,109,147:8,-1:3,147,-1,44,181:18,-1:17,181,-1:19,181:6" +
",44,181:11,-1:3,181,-1,181:2,174,181:16,-1:17,181,-1:19,181:5,174,181:12,-1" +
":3,181,-1,95:23,102,95:54,1,38:53,-1,38:22,-1,38,-1,97:47,69,97:5,-1:2,97:1" +
"9,-1,97:3,-1:54,103,-1:25,99:53,-1,99:22,-1,99,-1,147:13,43,147:4,43,-1:17," +
"147,-1:19,147:18,-1:3,147,-1,181,78,181:4,168,181:6,79,181:4,79,-1:17,181,-" +
"1:19,181:8,78,181:3,168,181:5,-1:3,181,-1:24,102,-1:3,106,-1:51,81,147:7,12" +
"5,147:10,-1:17,147,-1:19,125,147:5,81,147:11,-1:3,147,-1,181:13,80,181:4,80" +
",-1:17,181,-1:19,181:18,-1:3,181,-1:24,67,-1:55,147:10,51,147:8,-1:17,147,-" +
"1:19,147:15,51,147:2,-1:3,147,-1,181:5,84,181:10,84,181:2,-1:17,181,-1:19,1" +
"81:18,-1:3,181,-1,147:5,52,147:10,52,147:2,-1:17,147,-1:19,147:18,-1:3,147," +
"-1,181:10,82,181:8,-1:17,181,-1:19,181:15,82,181:2,-1:3,181,-1,147:8,127,14" +
"7:10,-1:17,147,-1:19,127,147:17,-1:3,147,-1,181:5,83,181:10,83,181:2,-1:17," +
"181,-1:19,181:18,-1:3,181,-1,147:6,129,147:12,-1:17,147,-1:19,147:12,129,14" +
"7:5,-1:3,147,-1,181,56,181:17,-1:17,181,-1:19,181:8,56,181:9,-1:3,181,-1,14" +
"7:5,53,147:10,53,147:2,-1:17,147,-1:19,147:18,-1:3,147,-1,181:3,88,181:15,-" +
"1:17,181,-1:19,181:4,88,181:13,-1:3,181,-1,147:11,134,147:7,-1:17,147,-1:19" +
",147:9,134,147:8,-1:3,147,-1,181:7,85,181:11,-1:17,181,-1:19,181:2,85,181:1" +
"5,-1:3,181,-1,152,147:18,-1:17,147,-1:19,147:6,152,147:11,-1:3,147,-1,181:3" +
",86,181:15,-1:17,181,-1:19,181:4,86,181:13,-1:3,181,-1,147:3,136,147:15,-1:" +
"17,147,-1:19,147:4,136,147:13,-1:3,147,-1,181:12,89,181:6,-1:17,181,-1:19,1" +
"81:10,89,181:7,-1:3,181,-1,147:17,137,147,-1:17,147,-1:19,147:13,137,147:4," +
"-1:3,147,-1,181:9,90,181:9,-1:17,181,-1:19,181:7,90,181:10,-1:3,181,-1,147:" +
"9,153,147:9,-1:17,147,-1:19,147:7,153,147:10,-1:3,147,-1,181:6,91,181:12,-1" +
":17,181,-1:19,181:12,91,181:5,-1:3,181,-1,147:7,54,147:11,-1:17,147,-1:19,1" +
"47:2,54,147:15,-1:3,147,-1,181:3,92,181:15,-1:17,181,-1:19,181:4,92,181:13," +
"-1:3,181,-1,147:3,55,147:15,-1:17,147,-1:19,147:4,55,147:13,-1:3,147,-1,181" +
":15,93,181:3,-1:17,181,-1:19,181:3,93,181:14,-1:3,181,-1,147:3,57,147:15,-1" +
":17,147,-1:19,147:4,57,147:13,-1:3,147,-1,181:6,94,181:12,-1:17,181,-1:19,1" +
"81:12,94,181:5,-1:3,181,-1,147:6,140,147:12,-1:17,147,-1:19,147:12,140,147:" +
"5,-1:3,147,-1,147:12,58,147:6,-1:17,147,-1:19,147:10,58,147:7,-1:3,147,-1,1" +
"47:9,59,147:9,-1:17,147,-1:19,147:7,59,147:10,-1:3,147,-1,147,87,147:17,-1:" +
"17,147,-1:19,147:8,87,147:9,-1:3,147,-1,147:3,60,147:15,-1:17,147,-1:19,147" +
":4,60,147:13,-1:3,147,-1,147:4,154,147:14,-1:17,147,-1:19,147:11,154,147:6," +
"-1:3,147,-1,143,147:18,-1:17,147,-1:19,147:6,143,147:11,-1:3,147,-1,147:6,6" +
"1,147:12,-1:17,147,-1:19,147:12,61,147:5,-1:3,147,-1,147:3,62,147:15,-1:17," +
"147,-1:19,147:4,62,147:13,-1:3,147,-1,147:3,63,147:15,-1:17,147,-1:19,147:4" +
",63,147:13,-1:3,147,-1,147:15,64,147:3,-1:17,147,-1:19,147:3,64,147:14,-1:3" +
",147,-1,147:5,145,147:10,145,147:2,-1:17,147,-1:19,147:18,-1:3,147,-1,147:6" +
",65,147:12,-1:17,147,-1:19,147:12,65,147:5,-1:3,147,-1,181:3,108,181:7,170," +
"181:7,-1:17,181,-1:19,181:4,108,181:4,170,181:8,-1:3,181,-1,147:8,133,147:1" +
"0,-1:17,147,-1:19,133,147:17,-1:3,147,-1,147:6,131,147:12,-1:17,147,-1:19,1" +
"47:12,131,147:5,-1:3,147,-1,147:11,135,147:7,-1:17,147,-1:19,147:9,135,147:" +
"8,-1:3,147,-1,147:3,138,147:15,-1:17,147,-1:19,147:4,138,147:13,-1:3,147,-1" +
",147:9,141,147:9,-1:17,147,-1:19,147:7,141,147:10,-1:3,147,-1,147:6,142,147" +
":12,-1:17,147,-1:19,147:12,142,147:5,-1:3,147,-1,144,147:18,-1:17,147,-1:19" +
",147:6,144,147:11,-1:3,147,-1,181:3,110,181:7,112,181:7,-1:17,181,-1:19,181" +
":4,110,181:4,112,181:8,-1:3,181,-1,147:6,111,147:2,113,147:9,-1:17,147,-1:1" +
"9,147:7,113,147:4,111,147:5,-1:3,147,-1,147:11,139,147:7,-1:17,147,-1:19,14" +
"7:9,139,147:8,-1:3,147,-1,181:3,114,181:15,-1:17,181,-1:19,181:4,114,181:13" +
",-1:3,181,-1,147:8,149,148,147:9,-1:17,147,-1:19,149,147:6,148,147:10,-1:3," +
"147,-1,181:6,116,181:12,-1:17,181,-1:19,181:12,116,181:5,-1:3,181,-1,147:3," +
"115,147:7,117,147:7,-1:17,147,-1:19,147:4,115,147:4,117,147:8,-1:3,147,-1,1" +
"81:8,173,181:10,-1:17,181,-1:19,173,181:17,-1:3,181,-1,147:2,119,147:16,-1:" +
"17,147,-1:19,147:5,119,147:12,-1:3,147,-1,181:8,118,181:10,-1:17,181,-1:19," +
"118,181:17,-1:3,181,-1,147:11,150,147:7,-1:17,147,-1:19,147:9,150,147:8,-1:" +
"3,147,-1,181:6,120,181:12,-1:17,181,-1:19,181:12,120,181:5,-1:3,181,-1,147:" +
"2,121,147,123,147:14,-1:17,147,-1:19,147:5,121,147:5,123,147:6,-1:3,147,-1," +
"181:14,175,181:4,-1:17,181,-1:19,181:14,175,181:3,-1:3,181,-1,147:14,157,14" +
"7:4,-1:17,147,-1:19,147:14,157,147:3,-1:3,147,-1,181:11,122,181:7,-1:17,181" +
",-1:19,181:9,122,181:8,-1:3,181,-1,181:11,124,181:7,-1:17,181,-1:19,181:9,1" +
"24,181:8,-1:3,181,-1,176,181:18,-1:17,181,-1:19,181:6,176,181:11,-1:3,181,-" +
"1,181:6,126,181:12,-1:17,181,-1:19,181:12,126,181:5,-1:3,181,-1,181:3,177,1" +
"81:15,-1:17,181,-1:19,181:4,177,181:13,-1:3,181,-1,181:11,178,181:7,-1:17,1" +
"81,-1:19,181:9,178,181:8,-1:3,181,-1,181:9,128,181:9,-1:17,181,-1:19,181:7," +
"128,181:10,-1:3,181,-1,181:4,179,181:14,-1:17,181,-1:19,181:11,179,181:6,-1" +
":3,181,-1,130,181:18,-1:17,181,-1:19,181:6,130,181:11,-1:3,181,-1,180,181:1" +
"8,-1:17,181,-1:19,181:6,180,181:11,-1:3,181,-1,181:5,132,181:10,132,181:2,-" +
"1:17,181,-1:19,181:18,-1:3,181,-1,181:8,160,162,181:9,-1:17,181,-1:19,160,1" +
"81:6,162,181:10,-1:3,181,-1,181:6,164,181:2,166,181:9,-1:17,181,-1:19,181:7" +
",166,181:4,164,181:5,-1:3,181,-1,181:11,171,181:7,-1:17,181,-1:19,181:9,171" +
",181:8,-1:3,181,-1,181:2,172,181:16,-1:17,181,-1:19,181:5,172,181:12,-1:3,1" +
"81");

	public java_cup.runtime.Symbol next_token ()
		throws java.io.IOException {
		int yy_lookahead;
		int yy_anchor = YY_NO_ANCHOR;
		int yy_state = yy_state_dtrans[yy_lexical_state];
		int yy_next_state = YY_NO_STATE;
		int yy_last_accept_state = YY_NO_STATE;
		boolean yy_initial = true;
		int yy_this_accept;

		yy_mark_start();
		yy_this_accept = yy_acpt[yy_state];
		if (YY_NOT_ACCEPT != yy_this_accept) {
			yy_last_accept_state = yy_state;
			yy_mark_end();
		}
		while (true) {
			if (yy_initial && yy_at_bol) yy_lookahead = YY_BOL;
			else yy_lookahead = yy_advance();
			yy_next_state = YY_F;
			yy_next_state = yy_nxt[yy_rmap[yy_state]][yy_cmap[yy_lookahead]];
			if (YY_EOF == yy_lookahead && true == yy_initial) {

/*  Stuff enclosed in %eofval{ %eofval} specifies java code that is
 *  executed when end-of-file is reached.  If you use multiple lexical
 *  states and want to do something special if an EOF is encountered in
 *  one of those states, place your code in the switch statement.
 *  Ultimately, you should return the EOF symbol, or your lexer won't
 *  work.  */
    switch(yy_lexical_state) {
    case YYINITIAL:
        break;
    case STRING:
        yybegin(END);
        return new Symbol(TokenConstants.ERROR, "EOF in string constant");
    case COMMENT:
        yybegin(END);
        return new Symbol(TokenConstants.ERROR, "EOF in comment");
    case END:
        return new Symbol(TokenConstants.EOF);
	/* nothing special to do in the initial state */
	/* If necessary, add code for other states here, e.g:
	   case COMMENT:
	   ...
	   break;
	*/
    }
    return new Symbol(TokenConstants.EOF);
			}
			if (YY_F != yy_next_state) {
				yy_state = yy_next_state;
				yy_initial = false;
				yy_this_accept = yy_acpt[yy_state];
				if (YY_NOT_ACCEPT != yy_this_accept) {
					yy_last_accept_state = yy_state;
					yy_mark_end();
				}
			}
			else {
				if (YY_NO_STATE == yy_last_accept_state) {
					throw (new Error("Lexical Error: Unmatched Input."));
				}
				else {
					yy_anchor = yy_acpt[yy_last_accept_state];
					if (0 != (YY_END & yy_anchor)) {
						yy_move_end();
					}
					yy_to_mark();
					switch (yy_last_accept_state) {
					case 0:
						{yybegin(YYINITIAL);}
					case -2:
						break;
					case 1:
						
					case -3:
						break;
					case 2:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -4:
						break;
					case 3:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -5:
						break;
					case 4:
						{curr_lineno++; return new Symbol(TokenConstants.EQ);}
					case -6:
						break;
					case 5:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, ">");}
					case -7:
						break;
					case 6:
						{curr_lineno++; return new Symbol(TokenConstants.LT);}
					case -8:
						break;
					case 7:
						{curr_lineno++; return new Symbol(TokenConstants.MINUS);}
					case -9:
						break;
					case 8:
						{curr_lineno++; return new Symbol(TokenConstants.MULT);}
					case -10:
						break;
					case 9:
						{curr_lineno++; return new Symbol(TokenConstants.SEMI);}
					case -11:
						break;
					case 10:
						{curr_lineno++; return new Symbol(TokenConstants.NEG);}
					case -12:
						break;
					case 11:
						{curr_lineno++; return new Symbol(TokenConstants.LPAREN);}
					case -13:
						break;
					case 12:
						{curr_lineno++; return new Symbol(TokenConstants.RPAREN);}
					case -14:
						break;
					case 13:
						{curr_lineno++; return new Symbol(TokenConstants.COMMA);}
					case -15:
						break;
					case 14:
						{curr_lineno++; return new Symbol(TokenConstants.DIV);}
					case -16:
						break;
					case 15:
						{curr_lineno++; return new Symbol(TokenConstants.PLUS);}
					case -17:
						break;
					case 16:
						{curr_lineno++; return new Symbol(TokenConstants.COLON);}
					case -18:
						break;
					case 17:
						{curr_lineno++; return new Symbol(TokenConstants.LBRACE);}
					case -19:
						break;
					case 18:
						{curr_lineno++; return new Symbol(TokenConstants.RBRACE);}
					case -20:
						break;
					case 19:
						{curr_lineno++; return new Symbol(TokenConstants.DOT);}
					case -21:
						break;
					case 20:
						{curr_lineno++; return new Symbol(TokenConstants.AT);}
					case -22:
						break;
					case 21:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "_");}
					case -23:
						break;
					case 22:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "!");}
					case -24:
						break;
					case 23:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "#");}
					case -25:
						break;
					case 24:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "$");}
					case -26:
						break;
					case 25:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "%");}
					case -27:
						break;
					case 26:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "^");}
					case -28:
						break;
					case 27:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "&");}
					case -29:
						break;
					case 28:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "?");}
					case -30:
						break;
					case 29:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "`");}
					case -31:
						break;
					case 30:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "[");}
					case -32:
						break;
					case 31:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "]");}
					case -33:
						break;
					case 32:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "\\");}
					case -34:
						break;
					case 33:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "|");}
					case -35:
						break;
					case 34:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "\001");}
					case -36:
						break;
					case 35:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "\002");}
					case -37:
						break;
					case 36:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "\003");}
					case -38:
						break;
					case 37:
						{curr_lineno++; return new Symbol(TokenConstants.ERROR, "\004");}
					case -39:
						break;
					case 38:
						{System.err.println("LEXER BUG - UNMATCHED: " + yytext()); }
					case -40:
						break;
					case 39:
						{curr_lineno++; return new Symbol(TokenConstants.INT_CONST, AbstractTable.inttable.addString(yytext()));}
					case -41:
						break;
					case 40:
						{curr_lineno++; yybegin(STRING);}
					case -42:
						break;
					case 41:
						{curr_lineno++; return new Symbol(TokenConstants.IN);}
					case -43:
						break;
					case 42:
						{curr_lineno++; return new Symbol(TokenConstants.IF);}
					case -44:
						break;
					case 43:
						{curr_lineno++; return new Symbol(TokenConstants.OF);}
					case -45:
						break;
					case 44:
						{curr_lineno++; return new Symbol(TokenConstants.FI);}
					case -46:
						break;
					case 45:
						{curr_lineno++; return new Symbol(TokenConstants.DARROW);}
					case -47:
						break;
					case 46:
						{curr_lineno++; return new Symbol(TokenConstants.LE);}
					case -48:
						break;
					case 47:
						{curr_lineno++; return new Symbol(TokenConstants.ASSIGN);}
					case -49:
						break;
					case 48:
						{curr_lineno++; yybegin(YYINITIAL);}
					case -50:
						break;
					case 49:
						{return new Symbol(TokenConstants.ERROR, "Unmatched *)");}
					case -51:
						break;
					case 50:
						{curr_lineno++; cont++; yybegin(COMMENT);}
					case -52:
						break;
					case 51:
						{curr_lineno++; return new Symbol(TokenConstants.NEW);}
					case -53:
						break;
					case 52:
						{curr_lineno++; return new Symbol(TokenConstants.NOT);}
					case -54:
						break;
					case 53:
						{curr_lineno++; return new Symbol(TokenConstants.LET);}
					case -55:
						break;
					case 54:
						{curr_lineno++; return new Symbol(TokenConstants.ESAC);}
					case -56:
						break;
					case 55:
						{curr_lineno++; return new Symbol(TokenConstants.ELSE);}
					case -57:
						break;
					case 56:
						{curr_lineno++; return new Symbol(TokenConstants.THEN);}
					case -58:
						break;
					case 57:
						{curr_lineno++; return new Symbol(TokenConstants.CASE);}
					case -59:
						break;
					case 58:
						{curr_lineno++; return new Symbol(TokenConstants.LOOP);}
					case -60:
						break;
					case 59:
						{curr_lineno++; return new Symbol(TokenConstants.POOL);}
					case -61:
						break;
					case 60:
						{curr_lineno++; return new Symbol(TokenConstants.BOOL_CONST, yytext()); }
					case -62:
						break;
					case 61:
						{curr_lineno++; return new Symbol(TokenConstants.CLASS);}
					case -63:
						break;
					case 62:
						{curr_lineno++; return new Symbol(TokenConstants.WHILE);}
					case -64:
						break;
					case 63:
						{curr_lineno++; return new Symbol(TokenConstants.BOOL_CONST, yytext()); }
					case -65:
						break;
					case 64:
						{curr_lineno++; return new Symbol(TokenConstants.ISVOID);}
					case -66:
						break;
					case 65:
						{curr_lineno++; return new Symbol(TokenConstants.INHERITS);}
					case -67:
						break;
					case 66:
						{
                                    linecount = yytext();
                                    while (linecount.contains("\n")){
                                        curr_lineno++;
                                        linecount = linecount.replaceFirst("\n",".");                   
                                    } 
                                    if (yytext().contains("(**)")) {
                                        yybegin(COMMENT);
                                    } else if (yytext().length()<2){
                                        yybegin(COMMENT); 
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("*)")) {
                                        aux = yytext();
                                        while (true){
                                            if (aux.length()>1){
                                                if (aux.substring(aux.length()-2,aux.length()).equals("*)")){
                                                    aux = aux.substring(0,aux.length()-2);
                                                    cont--;
                                                } else {
                                                    break;
                                                } 
                                            } else {
                                                break;
                                            }                                            
                                        }
                                        if (cont == 0){
                                            yybegin(YYINITIAL);
                                        } else {
                                            yybegin(COMMENT);
                                        }                                        
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("(*")){
                                        cont++;
                                    } else {
                                        yybegin(COMMENT); 
                                    }                                                                    
                                }
					case -68:
						break;
					case 68:
						{str=str.concat(yytext());
                                                if (str.contains("\\n")){
                                                    str = str.replace("\\n","\n");                                                    
                                                }
                                                if (str.contains("\\t")){
                                                    str = str.replace("\\t","\t");
                                                }
                                                if (str.contains("\\f")){
                                                    str = str.replace("\\f","\f");
                                                }
                                                if (str.contains("\\b")){
                                                    str = str.replace("\\b","\b");
                                                }                                    
                                        }
					case -69:
						break;
					case 69:
						{                                    
                                            pos = yytext().length();
                                            if (pos>=2){
                                                s = yytext().substring(pos-1,pos);
                                            }                                        
                                            if (pos>1 && (!(s.equals("t") || s.equals("f") || s.equals("b") || s.equals("n") || s.equals("\"")))) {
                                                if (yytext().substring(pos-2,pos).equals("\\\n")){
                                                    str=str.concat(yytext().substring(0,pos-2).concat("\n"));
                                                    nl = true;
                                                } else {
                                                    str=str.concat(yytext().substring(0,pos-1));
                                                }
                                            } else if (pos>1){                                               
                                                str=str.concat(yytext());
                                                if (str.contains("\\n")){
                                                    str = str.replace("\\n","\n");                                                    
                                                }
                                                if (str.contains("\\t")){
                                                    str = str.replace("\\t","\t");
                                                }
                                                if (str.contains("\\f")){
                                                    str = str.replace("\\f","\f");
                                                }
                                                if (str.contains("\\b")){
                                                    str = str.replace("\\b","\b");
                                                }
                                            }
                                            if (pos>1){
                                                if (yytext().equals("\\\n")){
                                                } else if (str.substring(str.length()-1,str.length()).equals("\\")){
                                                    nl = true;
                                                }
                                            }                                                                                 
                                        }
					case -70:
						break;
					case 70:
						{  
                                            if (nl==true){
                                                str=str.concat("\n");
                                                nl = false;
                                            } else if (str.length()>0){
                                                if (nl == false && !str.substring(str.length()-1,str.length()).equals("\\")){
                                                    yybegin(YYINITIAL);
                                                    nl = false;                                                
                                                    str = "";  
                                                    return new Symbol(TokenConstants.ERROR, "Unterminated string constant");  
                                                }                                                                                               
                                            } 
                                            if (nl == false){
                                                yybegin(YYINITIAL);
                                                nl = false;                                                
                                                str = "";
                                                return new Symbol(TokenConstants.ERROR, "Unterminated string constant");                                                
                                            }                                            
                                        }
					case -71:
						break;
					case 71:
						{                                                
                                            ret=str; 
                                            str=""; 
                                            yybegin(YYINITIAL); 
                                            nl = false;
                                            if (ret.length()>=MAX_STR_CONST){
                                                yybegin(ERROR_STRING1);
                                            } else {
                                                return new Symbol(TokenConstants.STR_CONST, AbstractTable.stringtable.addString(ret));
                                            }                                            
                                        }
					case -72:
						break;
					case 72:
						{ 
                                            if (yytext().length()>1) {
                                                str=str.concat(yytext().substring(0,yytext().length()-1));
                                            }
                                        }
					case -73:
						break;
					case 73:
						{   
                                            pos = yytext().length();
                                            if (pos>1 && yytext().substring(pos-2,pos).equals("\\\"")) {
                                                nl = true;
                                                if (yytext().equals("\\\"")){
                                                    str=str.concat(yytext().substring(0,pos-2).concat("\""));
                                                } else if (yytext().contains("\\\"")){
                                                    str = str.concat(yytext());
                                                    str = str.replace("\\\"","\"");
                                                } else {
                                                    str = str.concat(yytext());
                                                }                                             
                                            }
                                        }
					case -74:
						break;
					case 74:
						{yybegin(YYINITIAL); return new Symbol(TokenConstants.ERROR, "String constant too long");}
					case -75:
						break;
					case 75:
						{yybegin(YYINITIAL);}
					case -76:
						break;
					case 76:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -77:
						break;
					case 77:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -78:
						break;
					case 78:
						{curr_lineno++; return new Symbol(TokenConstants.IN);}
					case -79:
						break;
					case 79:
						{curr_lineno++; return new Symbol(TokenConstants.IF);}
					case -80:
						break;
					case 80:
						{curr_lineno++; return new Symbol(TokenConstants.OF);}
					case -81:
						break;
					case 81:
						{curr_lineno++; return new Symbol(TokenConstants.FI);}
					case -82:
						break;
					case 82:
						{curr_lineno++; return new Symbol(TokenConstants.NEW);}
					case -83:
						break;
					case 83:
						{curr_lineno++; return new Symbol(TokenConstants.NOT);}
					case -84:
						break;
					case 84:
						{curr_lineno++; return new Symbol(TokenConstants.LET);}
					case -85:
						break;
					case 85:
						{curr_lineno++; return new Symbol(TokenConstants.ESAC);}
					case -86:
						break;
					case 86:
						{curr_lineno++; return new Symbol(TokenConstants.ELSE);}
					case -87:
						break;
					case 87:
						{curr_lineno++; return new Symbol(TokenConstants.THEN);}
					case -88:
						break;
					case 88:
						{curr_lineno++; return new Symbol(TokenConstants.CASE);}
					case -89:
						break;
					case 89:
						{curr_lineno++; return new Symbol(TokenConstants.LOOP);}
					case -90:
						break;
					case 90:
						{curr_lineno++; return new Symbol(TokenConstants.POOL);}
					case -91:
						break;
					case 91:
						{curr_lineno++; return new Symbol(TokenConstants.CLASS);}
					case -92:
						break;
					case 92:
						{curr_lineno++; return new Symbol(TokenConstants.WHILE);}
					case -93:
						break;
					case 93:
						{curr_lineno++; return new Symbol(TokenConstants.ISVOID);}
					case -94:
						break;
					case 94:
						{curr_lineno++; return new Symbol(TokenConstants.INHERITS);}
					case -95:
						break;
					case 95:
						{
                                    linecount = yytext();
                                    while (linecount.contains("\n")){
                                        curr_lineno++;
                                        linecount = linecount.replaceFirst("\n",".");                   
                                    } 
                                    if (yytext().contains("(**)")) {
                                        yybegin(COMMENT);
                                    } else if (yytext().length()<2){
                                        yybegin(COMMENT); 
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("*)")) {
                                        aux = yytext();
                                        while (true){
                                            if (aux.length()>1){
                                                if (aux.substring(aux.length()-2,aux.length()).equals("*)")){
                                                    aux = aux.substring(0,aux.length()-2);
                                                    cont--;
                                                } else {
                                                    break;
                                                } 
                                            } else {
                                                break;
                                            }                                            
                                        }
                                        if (cont == 0){
                                            yybegin(YYINITIAL);
                                        } else {
                                            yybegin(COMMENT);
                                        }                                        
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("(*")){
                                        cont++;
                                    } else {
                                        yybegin(COMMENT); 
                                    }                                                                    
                                }
					case -96:
						break;
					case 97:
						{str=str.concat(yytext());
                                                if (str.contains("\\n")){
                                                    str = str.replace("\\n","\n");                                                    
                                                }
                                                if (str.contains("\\t")){
                                                    str = str.replace("\\t","\t");
                                                }
                                                if (str.contains("\\f")){
                                                    str = str.replace("\\f","\f");
                                                }
                                                if (str.contains("\\b")){
                                                    str = str.replace("\\b","\b");
                                                }                                    
                                        }
					case -97:
						break;
					case 98:
						{                                    
                                            pos = yytext().length();
                                            if (pos>=2){
                                                s = yytext().substring(pos-1,pos);
                                            }                                        
                                            if (pos>1 && (!(s.equals("t") || s.equals("f") || s.equals("b") || s.equals("n") || s.equals("\"")))) {
                                                if (yytext().substring(pos-2,pos).equals("\\\n")){
                                                    str=str.concat(yytext().substring(0,pos-2).concat("\n"));
                                                    nl = true;
                                                } else {
                                                    str=str.concat(yytext().substring(0,pos-1));
                                                }
                                            } else if (pos>1){                                               
                                                str=str.concat(yytext());
                                                if (str.contains("\\n")){
                                                    str = str.replace("\\n","\n");                                                    
                                                }
                                                if (str.contains("\\t")){
                                                    str = str.replace("\\t","\t");
                                                }
                                                if (str.contains("\\f")){
                                                    str = str.replace("\\f","\f");
                                                }
                                                if (str.contains("\\b")){
                                                    str = str.replace("\\b","\b");
                                                }
                                            }
                                            if (pos>1){
                                                if (yytext().equals("\\\n")){
                                                } else if (str.substring(str.length()-1,str.length()).equals("\\")){
                                                    nl = true;
                                                }
                                            }                                                                                 
                                        }
					case -98:
						break;
					case 99:
						{yybegin(YYINITIAL); return new Symbol(TokenConstants.ERROR, "String constant too long");}
					case -99:
						break;
					case 100:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -100:
						break;
					case 101:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -101:
						break;
					case 102:
						{
                                    linecount = yytext();
                                    while (linecount.contains("\n")){
                                        curr_lineno++;
                                        linecount = linecount.replaceFirst("\n",".");                   
                                    } 
                                    if (yytext().contains("(**)")) {
                                        yybegin(COMMENT);
                                    } else if (yytext().length()<2){
                                        yybegin(COMMENT); 
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("*)")) {
                                        aux = yytext();
                                        while (true){
                                            if (aux.length()>1){
                                                if (aux.substring(aux.length()-2,aux.length()).equals("*)")){
                                                    aux = aux.substring(0,aux.length()-2);
                                                    cont--;
                                                } else {
                                                    break;
                                                } 
                                            } else {
                                                break;
                                            }                                            
                                        }
                                        if (cont == 0){
                                            yybegin(YYINITIAL);
                                        } else {
                                            yybegin(COMMENT);
                                        }                                        
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("(*")){
                                        cont++;
                                    } else {
                                        yybegin(COMMENT); 
                                    }                                                                    
                                }
					case -102:
						break;
					case 103:
						{                                    
                                            pos = yytext().length();
                                            if (pos>=2){
                                                s = yytext().substring(pos-1,pos);
                                            }                                        
                                            if (pos>1 && (!(s.equals("t") || s.equals("f") || s.equals("b") || s.equals("n") || s.equals("\"")))) {
                                                if (yytext().substring(pos-2,pos).equals("\\\n")){
                                                    str=str.concat(yytext().substring(0,pos-2).concat("\n"));
                                                    nl = true;
                                                } else {
                                                    str=str.concat(yytext().substring(0,pos-1));
                                                }
                                            } else if (pos>1){                                               
                                                str=str.concat(yytext());
                                                if (str.contains("\\n")){
                                                    str = str.replace("\\n","\n");                                                    
                                                }
                                                if (str.contains("\\t")){
                                                    str = str.replace("\\t","\t");
                                                }
                                                if (str.contains("\\f")){
                                                    str = str.replace("\\f","\f");
                                                }
                                                if (str.contains("\\b")){
                                                    str = str.replace("\\b","\b");
                                                }
                                            }
                                            if (pos>1){
                                                if (yytext().equals("\\\n")){
                                                } else if (str.substring(str.length()-1,str.length()).equals("\\")){
                                                    nl = true;
                                                }
                                            }                                                                                 
                                        }
					case -103:
						break;
					case 104:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -104:
						break;
					case 105:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -105:
						break;
					case 106:
						{
                                    linecount = yytext();
                                    while (linecount.contains("\n")){
                                        curr_lineno++;
                                        linecount = linecount.replaceFirst("\n",".");                   
                                    } 
                                    if (yytext().contains("(**)")) {
                                        yybegin(COMMENT);
                                    } else if (yytext().length()<2){
                                        yybegin(COMMENT); 
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("*)")) {
                                        aux = yytext();
                                        while (true){
                                            if (aux.length()>1){
                                                if (aux.substring(aux.length()-2,aux.length()).equals("*)")){
                                                    aux = aux.substring(0,aux.length()-2);
                                                    cont--;
                                                } else {
                                                    break;
                                                } 
                                            } else {
                                                break;
                                            }                                            
                                        }
                                        if (cont == 0){
                                            yybegin(YYINITIAL);
                                        } else {
                                            yybegin(COMMENT);
                                        }                                        
                                    } else if (yytext().substring(yytext().length()-2,yytext().length()).equals("(*")){
                                        cont++;
                                    } else {
                                        yybegin(COMMENT); 
                                    }                                                                    
                                }
					case -106:
						break;
					case 107:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -107:
						break;
					case 108:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -108:
						break;
					case 109:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -109:
						break;
					case 110:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -110:
						break;
					case 111:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -111:
						break;
					case 112:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -112:
						break;
					case 113:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -113:
						break;
					case 114:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -114:
						break;
					case 115:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -115:
						break;
					case 116:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -116:
						break;
					case 117:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -117:
						break;
					case 118:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -118:
						break;
					case 119:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -119:
						break;
					case 120:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -120:
						break;
					case 121:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -121:
						break;
					case 122:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -122:
						break;
					case 123:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -123:
						break;
					case 124:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -124:
						break;
					case 125:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -125:
						break;
					case 126:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -126:
						break;
					case 127:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -127:
						break;
					case 128:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -128:
						break;
					case 129:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -129:
						break;
					case 130:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -130:
						break;
					case 131:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -131:
						break;
					case 132:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -132:
						break;
					case 133:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -133:
						break;
					case 134:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -134:
						break;
					case 135:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -135:
						break;
					case 136:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -136:
						break;
					case 137:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -137:
						break;
					case 138:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -138:
						break;
					case 139:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -139:
						break;
					case 140:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -140:
						break;
					case 141:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -141:
						break;
					case 142:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -142:
						break;
					case 143:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -143:
						break;
					case 144:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -144:
						break;
					case 145:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -145:
						break;
					case 146:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -146:
						break;
					case 147:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -147:
						break;
					case 148:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -148:
						break;
					case 149:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -149:
						break;
					case 150:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -150:
						break;
					case 151:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -151:
						break;
					case 152:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -152:
						break;
					case 153:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -153:
						break;
					case 154:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -154:
						break;
					case 155:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -155:
						break;
					case 156:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -156:
						break;
					case 157:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -157:
						break;
					case 158:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -158:
						break;
					case 159:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -159:
						break;
					case 160:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -160:
						break;
					case 161:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -161:
						break;
					case 162:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -162:
						break;
					case 163:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -163:
						break;
					case 164:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -164:
						break;
					case 165:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -165:
						break;
					case 166:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -166:
						break;
					case 167:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -167:
						break;
					case 168:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -168:
						break;
					case 169:
						{curr_lineno++; return new Symbol(TokenConstants.OBJECTID, AbstractTable.idtable.addString(yytext()));}
					case -169:
						break;
					case 170:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -170:
						break;
					case 171:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -171:
						break;
					case 172:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -172:
						break;
					case 173:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -173:
						break;
					case 174:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -174:
						break;
					case 175:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -175:
						break;
					case 176:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -176:
						break;
					case 177:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -177:
						break;
					case 178:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -178:
						break;
					case 179:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -179:
						break;
					case 180:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -180:
						break;
					case 181:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -181:
						break;
					case 182:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -182:
						break;
					case 183:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -183:
						break;
					case 184:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -184:
						break;
					case 185:
						{curr_lineno++; return new Symbol(TokenConstants.TYPEID, AbstractTable.idtable.addString(yytext()));}
					case -185:
						break;
					default:
						yy_error(YY_E_INTERNAL,false);
					case -1:
					}
					yy_initial = true;
					yy_state = yy_state_dtrans[yy_lexical_state];
					yy_next_state = YY_NO_STATE;
					yy_last_accept_state = YY_NO_STATE;
					yy_mark_start();
					yy_this_accept = yy_acpt[yy_state];
					if (YY_NOT_ACCEPT != yy_this_accept) {
						yy_last_accept_state = yy_state;
						yy_mark_end();
					}
				}
			}
		}
	}
}
